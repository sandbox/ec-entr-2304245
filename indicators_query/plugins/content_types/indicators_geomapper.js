/**
 * @file
 * Indicators Geomapper JS implementation.
 *
 *  Coded on 2014-05-16.
 *
 *  Display a Mapper.js map with its controls.
 *  When user clicks on a region, the DOM element
 *  (as defined by the variable indicators_input_geo_id), is updated
 */

// This function is called when html body is loaded.
function indicators_geo_init()
{
}

// Base path where to go.
var redirect_to;


(function($) {

  /**
   * OnClick event.
   */
  Drupal.indicators_query = {

    popup_country : function(country_code) {
      window.location = redirect_to + country_code;
    },

  }
})(jQuery);


(function ($) {

  /**
   * Attach click behavior to the map.
   */
  Drupal.behaviors.indicators_query = {
    // Settings:
    // - 'geo_id' DOM id of the option list to update on a click. If empty, the user will be redirected to 'path',
    // - 'map'  Map file,
    // - 'data' Map descriptor and display parameters file,
    // - 'path' url (absolute or internal) where to go when user clicks on a region. Region code will be appended to it.
    attach: function (context, settings) {
      $('.geo-picker-map-content area').click(function() {
        country_code = $(this).attr('id').substring(0,2);
        // 'geo_id' is empty. Redirect to 'path'
        if (settings.indicators_query.geo_id == '') {
          redirect_to = settings.indicators_query.path;
          Drupal.indicators_query.popup_country(country_code);
        }
        // Update geo_id
        else {
          // Update the indicators dropdown, propagate the event.
          $('#' + settings.indicators_query.geo_id).val(country_code).change();
        }
      });
    }
  };

})(jQuery);
