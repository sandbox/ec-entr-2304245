<?php

/**
 * @file
 * Provide Indicators Query and Display Filter.
 *
 * It is similar to Views exposed filter.
 */

/**
 * CTools info.
 *
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'single' => TRUE,
  'title' => t('Indicators Filter'),
  'icon' => 'icon_indicators.png',
  'description' => t('Show an exposed filter for a Statistical Data Query and Display.'),
  'category' => t('Indicators'),
  'edit form' => 'indicators_query_qdx_edit_form',
  'render callback' => 'indicators_query_qdx_render',
  'admin info' => 'indicators_query_qdx_admin_info',
  'defaults' => array(
    'qd_tid' => 0,
    'help' => '',
    'x_axis' => '',
    'order_default' => '',
    'sort_default' => '',
    'labels' => '',
    'options' => '',
    'auto_submit' => '',
    'load_default' => '',
  ),
);

/**
 * CTools info.
 *
 * Admin info callback for panel pane.
 */
function indicators_query_qdx_admin_info($subtype, $conf, $contexts) {
  if (!empty($conf)) {
    if (!empty($conf['qd_tid'])) {
      $qd = _indicators_load_term_uuid($conf['qd_tid']);
      if ($qd) {
        $name = $qd->name;
      }
      else {
        $name = t('Illegal Query and Display term id!');
      }
      $block = new stdClass();
      $block->title = t('Query and Display Filter for %name.', array(
        '%name' => $name,
      ));
      $block->content = '';
      return $block;
    }
  }
}

/**
 * CTools info.
 *
 * Edit form callback for the content type.
 */
function indicators_query_qdx_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];

  $dropdown_array = _indicators_terms_as_options(INDICATORS_QUERY_VOC_QD, 'uuid');

  $form['qd_tid'] = array(
    '#title' => t('Query and Display'),
    '#description' => t('Term of !link vocabulary.', array('!link' => l(t('Eurostat Queries and Displays'), 'admin/structure/taxonomy/' . INDICATORS_QUERY_VOC_QD))),
    '#type' => 'select',
    '#options' => $dropdown_array,
    '#default_value' => $conf['qd_tid'],
    '#required' => TRUE,
  );

  $form['help'] = array(
    '#title' => t('Additional text to display'),
    '#description' => t('This text will be displayed below the title.'),
    '#type' => 'textfield',
    '#default_value' => $conf['help'],
    '#required' => FALSE,
  );

  $form['order_default'] = array(
    '#title' => t('Default sorting field'),
    '#description' => t('The label of the field on which the Q&D result will be sorted by default. It can also be a default field: Geo, Time, Unit.'),
    '#type' => 'textfield',
    '#default_value' => $conf['order_default'],
    '#required' => TRUE,
  );

  $form['sort_default'] = array(
    '#title' => t('Default sorting order'),
    '#description' => t('This sorting order will apply to the field above.'),
    '#type' => 'select',
    '#options' => array('ASC' => t('Ascending'), 'DESC' => t('Descending')),
    '#default_value' => $conf['sort_default'],
    '#required' => TRUE,
  );

  $form['labels'] = array(
    '#title' => t('Filter labels'),
    '#description' => t('These labels will be used instead of the default (vocabulary names). Separate labels with | (vertical bar).'),
    '#type' => 'textfield',
    '#default_value' => $conf['labels'],
    '#required' => TRUE,
  );

  $form['options'] = array(
    '#title' => t('Filter options'),
    '#description' => t('These options will be used to order filter option lists. They correspond to the filter labels above. Format of an option: ASC or DESC. An optional :none can be added to generate -none- item in the list. Separate options with | (vertical bar).'),
    '#type' => 'textfield',
    '#default_value' => $conf['options'],
    '#required' => TRUE,
  );

  $form['auto_submit'] = array(
    '#title' => t('Auto submit'),
    '#description' => t('If you choose Yes, the form will be submitted automatically if there is no more empty fields. The Submit button will be hidden.'),
    '#type' => 'checkbox',
    '#default_value' => $conf['auto_submit'],
    '#required' => FALSE,
  );

  $form['load_default'] = array(
    '#title' => t('Load default values of filters'),
    '#description' => t('If you choose Yes, all unknown filter values will be set to the first value in their options list.'),
    '#type' => 'checkbox',
    '#default_value' => $conf['load_default'],
    '#required' => FALSE,
  );

  return $form;
}

/**
 * The submit form stores the data in $conf.
 */
function indicators_query_qdx_edit_form_submit($form, &$form_state) {
  foreach (array_keys($form_state['plugin']['defaults']) as $key) {
    if (isset($form_state['values'][$key])) {
      $form_state['conf'][$key] = $form_state['values'][$key];
    }
  }
}

/**
 * Run-time rendering of the body of the block (content type).
 *
 * See ctools_plugin_examples for more advanced info.
 */
function indicators_query_qdx_render($subtype, $conf, $panel_args, $context = NULL) {
  $block = new stdClass();

  // Initial content is blank.
  $block->title = '';
  $block->content = '';
  $filters = array();
  $indicator = NULL;
  $url_query = drupal_get_query_parameters();

  $qd_tid = $conf['qd_tid'];
  // No Q&D provided? Get it from url.
  if (empty($qd_tid)) {
    if (isset($url_query['qd']) && is_numeric($url_query['qd'])) {
      $qd_tid = check_plain($url_query['qd']);
    }
  }
  // If no Q&D, it is up to the user to choose.
  if (!empty($qd_tid)) {
    $qd = _indicators_load_term_uuid($qd_tid);
    if (!$qd || empty($qd->field_es_indicator[LANGUAGE_NONE][0]['tid'])) {
      return $block;
    }

    $indicator = taxonomy_term_load($qd->field_es_indicator[LANGUAGE_NONE][0]['tid']);
    if (!$indicator) {
      return $block;
    }

    // Get filter labels.
    $field_labels = empty($conf['labels']) ? array() : explode('|', $conf['labels']);
    // Allowed operators on filters.
    $operators = indicators_query_sql_operators();
    // Get list of available filters.
    if (!empty($qd->field_es_source_filters[LANGUAGE_NONE])) {
      $i = 0;
      foreach ($qd->field_es_source_filters[LANGUAGE_NONE] as $filter_info) {
        // Check if 'time' filter has been overwritten in the configuration.
        if (!empty($conf['overwrite_time_filter']) && substr($filter_info['value'], 0, 5) == 'time=') {
          $filter_info['value'] = $conf['overwrite_time_filter'];
        }
        foreach ($operators as $operator) {
          // [0] = "age", [1] = "TOTAL" ?
          $filter = explode($operator, $filter_info['value']);
          // Operator strictly matches?
          if (count($filter) == 2) {
            $field_name = $filter[0];
            $field_value = $filter[1];
            // filter's value is not fixed.
            if (substr($field_value, 0, 1) == '%') {
              $filters[$field_name] = indicators_query_data_field_info($indicator, $field_name);
              // Any label appended?
              // [0] = "%geo", [1] = "Region".
              $field_info = explode(':', $field_value);
              if (!empty($field_info[1])) {
                $filters[$field_name]['label'] = t(trim($field_info[1]));
                $filters[$field_name]['query'] = substr($field_info[0], 1);
              }
              else {
                $filters[$field_name]['query'] = substr($field_value, 1);
              }
              // Any custom label (from panel)?
              // It will override label from Q&D term.
              if (isset($field_labels[$i])) {
                $filters[$field_name]['label'] = t(trim($field_labels[$i]));
              }
              ++$i;
            }
            // All values.
            elseif ($field_value == '*') {
            }
            break;
          }
        }
      }
    }
  }

  // Sorting options.
  $options = array(
    'sort' => $conf['sort_default'],
    'order' => $conf['order_default'],
    'options' => $conf['options'],
    'qd_tid' => $qd->tid,
    'auto_submit' => $conf['auto_submit'],
  );

  if (!empty($filters)) {
    // Create form.
    $form = drupal_get_form('indicators_query_filter_form', $filters, $indicator, $options);

    // Check if all filter values are present.
    // If not, load the default values by going to a new url.
    if (!empty($conf['load_default'])) {
      indicators_query_filter_load_defaults($form, $filters);
    }
    // Add rendered form to block.
    $block->content = drupal_render($form);
  }
  else {
    $block->content = t('You have not defined any filter in !qd.', array('!qd' => l(t('Query and Display'), 'taxonomy/term/' . $qd->tid)));
  }

  // Add themed filter to block.
  $block->content .= theme('indicators_query_filter', array(
        'help' => t($conf['help']),
      ));

  $block->content = '<div class="content">' . $block->content . "</div>\n";
  return $block;
}


/**
 * Build form with filter fields.
 *
 * @param array $filters
 *   Query filters.
 * @param object $indicator
 *   Indicator ot NULL.
 */
function indicators_query_filter_form($form, &$form_state, array $filters, $indicator, $options) {
  // The path to which the form will be submitted.
  $query = array();
  $url_query = drupal_get_query_parameters();

  // Get Sorting options.
  foreach (array('sort', 'order') as $sort_option) {
    if (isset($url_query[$sort_option])) {
      $query[$sort_option] = check_plain($url_query[$sort_option]);
    }
    elseif (isset($options[$sort_option])) {
      $query[$sort_option] = $options[$sort_option];
    }
  }
  if (!empty($options['options'])) {
    $options_list = explode('|', $options['options']);
  }

  if ($indicator) {
    $i = 0;
    // An input field for each filter field.
    foreach ($filters as $filter => $filter_info) {
      // Get options list options for the filter.
      $sort = 'ASC';
      $none = TRUE;
      if (isset($options_list[$i])) {
        $opt = explode(':', $options_list[$i]);
        if (isset($opt[0])) {
          $sort = strtoupper(trim($opt[0]));
        }
        if (isset($opt[1])) {
          $none = strtolower(trim($opt[1])) == 'none' ? TRUE : FALSE;
        }
        else {
          $none = FALSE;
        }
      }

      $qd_tid = isset($options['qd_tid']) ? $options['qd_tid'] : 0;

      // Get all field's option values available from the query.
      $dropdown_array = indicators_query_data_field_options($indicator->tid, $filter, $indicator->field_es_data_table[LANGUAGE_NONE][0]['value'], $sort, 'value', $none, $qd_tid);
      if (!$dropdown_array) {
        break;
      }
      $default_value = '';
      // Get field's default value from posted values.
      if (isset($form_state['values'][$filter])) {
        $default_value = $form_state['values'][$filter];
      }
      // Get field's default value from url.
      elseif (isset($url_query[$filter_info['query']])) {
        $default_value = check_plain($url_query[$filter_info['query']]);
      }
      // Validate the default value.
      if (!in_array($default_value, array_keys($dropdown_array))) {
        // The options list maybe a nested array.
        $option_found = FALSE;
        foreach ($dropdown_array as $option) {
          if (is_array($option) && in_array($default_value, array_keys($option))) {
            $option_found = TRUE;
          }
        }
        if (!$option_found) {
          // Invalid, reset to void.
          $default_value = '';
        }
      }

      $form[$filter] = array(
        '#title' => check_plain($filter_info['label']),
        '#type' => 'select',
        '#options' => $dropdown_array,
        '#default_value' => $default_value,
        '#required' => FALSE,
      );
      // Add auto_submit eventually.
      if (!empty($options['auto_submit'])) {
        $form[$filter]['#attributes'] = array(
          'onChange' => "document.getElementById('indicators-query-filter-form').submit();",
        );
      }

      // The filter's value will be added to the url later.
      if ($default_value) {
        $query[$filter_info['query']] = $default_value;
      }
      ++$i;
    }
  }
  // No Q&D was provided, let the user to choose one first.
  else {
    $dropdown_array = _indicators_terms_as_options(INDICATORS_QUERY_VOC_QD, 'uuid');
    $form['qd'] = array(
      '#title' => t('Indicator'),
      '#type' => 'select',
      '#options' => $dropdown_array,
      '#default_value' => 0,
      '#required' => FALSE,
    );
  }

  // Add Submit button.
  $form['actions'] = array(
    '#type' => 'actions',
    '#attributes' => array('class' => array('indicator-filter')),
  );
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Search'),
  );
  // If autosubmit, hide Submit button.
  if (!empty($options['auto_submit'])) {
    $form['actions']['submit']['#prefix'] = '<span class= "element-invisible">';
    $form['actions']['submit']['#suffix'] = '</span>';
  }

  // qd_tid already in the url? add it.
  if (isset($url_query['qd']) && is_numeric($url_query['qd'])) {
    $query['qd'] = check_plain($url_query['qd']);
  }

  // Add 'geo' field if not present already.
  if (empty($form['geo']) && !empty($url_query['geo'])) {
    $form['geo'] = array(
      '#type' => 'hidden',
      '#value' => check_plain($url_query['geo']),
    );
    $query['geo'] = $form['geo']['#value'];
    unset($query['form_token']);
  }

  // Add sort and filter values to the url, to go to at submit.
  $form['#action'] = url(request_path(), array('query' => $query, 'absolute' => TRUE));

  return $form;
}

/**
 * Submit filter values.
 *
 * Simply add filter values to the url and redirect.
 */
function indicators_query_filter_form_submit($form, &$form_state) {
  // Q&D has just been chosen.
  if (isset($form_state['values']['qd'])) {
    drupal_goto($form['#action'], array('query' => array('qd' => $form_state['values']['qd'])));
  }

  // Check presence of all submitted fields in the query string.
  $url = drupal_parse_url($form['#action']);
  foreach ($form_state['values'] as $key => $value) {
    // Except these fields:
    if (!in_array($key, array(
      'form_build_id',
      'form_id',
      'form_token',
      'cancel',
      'submit',
      'op',
      ))) {
      // Add or change the field's value.
      $url['query'][$key] = $value;
    }
  }

  // URL.
  $path = $url['path'];
  // URL options (query and fragment).
  unset($url['path']);
  drupal_goto($path, $url);
}

/**
 * Reload the page with default filter values.
 *
 * @param array $filters
 *   Query filters.
 */
function indicators_query_filter_load_defaults(&$form, array $filters) {
  $option_added = FALSE;
  $query = drupal_get_query_parameters();
  // An input field for each filter field.
  foreach ($filters as $filter => $filter_info) {
    if (!isset($query[$filter_info['query']])) {
      // Not set, add a default value.
      if (!empty($form[$filter]['#options'])) {
        $options = array_values($form[$filter]['#options']);
        // Get first option as default filter's value.
        $query[$filter] = $options[0];
        $option_added = TRUE;
      }
    }
  }
  // If any filter added to the query, go to that url.
  if ($option_added) {
    drupal_goto(request_path(), array('query' => $query));
  }
}
