<?php

/**
 * @file
 * Default theme implementation to display a statistical data filter block.
 *
 * Available variables:
 * - $help: Additional text to display.
 * - $info: Info about the executed Query (array):
 *     - filters (array)
 *     - pager (array)
 *     - dimensions (int)
 *     - axis (array)
 *
 * @ingroup themeable
 */
?>
<div class="es-qd-filter">

<?php if ($help): ?>
  <div class="help"><?php print $help; ?></div>
<?php endif;?>

</div>
