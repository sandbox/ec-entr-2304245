/**
 * @file
 * Indicators OpenLayers JS implementation.
 *
 *  Coded on 2013-05-19.
 *
 *  Display an OpenLayers map with its controls.
 *  When user clicks on a region, the DOM element geo_id is updated, or user is redirected to another url.
 *
 * These variables are defined in the settings, for example:
 *   geo_id = 'country'
 *   max_nuts_lvl = '3';
 *   geo_url = '/rim/sites/all/libraries/geomaps/europe_nuts0123.geojson';
 *   geo_lat = 56;
 *   geo_lon = 9;
 *   geo_zoom = 4;
 *   geo_use_google_osm = true;
 *   geo_use_google_satellite = false;
 *   geo_use_google_terrain = false;
 *   geo_use_google_roadmap = false;
 *   geo_use_google_hybrid = false;
 */

// Base path where to go.
var redirect_to;

var  epsg4326 = new OpenLayers.Projection('EPSG:4326'),
  epsg900913 = new OpenLayers.Projection('EPSG:900913');

// In these 3 vars we keep all polygons/features corresponding to the NUTS0,1,2,3 levels, per country (arrays of arrays)
var arrLX = new Array();  /* L0.. L3 */
arrLX[0] = new Array();
arrLX[1] = new Array();
arrLX[2] = new Array();
arrLX[3] = new Array();

// To keep current active country and its NUTS level.
var curr_nuts0_id = '';
var curr_nuts_lvl = 0;

// This function is called when html body is loaded.
function indicators_geo_init()
{
}


(function($) {

  /**
   * OnClick event.
   */
  Drupal.indicators_query = {

    popup_country : function(country_code) {
      window.location = redirect_to + country_code;
    },

  }
})(jQuery);

(function ($) {

  /**
   * Attach click behavior to the map.
   */
  Drupal.behaviors.indicators_query = {
    // Settings:
    // - 'geo_id' DOM id of the option list to update on a click. If empty, the user will be redirected to 'path'
    // - 'path' url (absolute or internal) where to go when user clicks on a region. Region code will be appended to it
    // - ...
    attach: function (context, settings) {

      // Instantiate the OL map object.
      var map = new OpenLayers.Map('map', {
        units: 'm',
        numZoomLevels: 19,
        controls: [
          new OpenLayers.Control.Navigation(),
          new OpenLayers.Control.ScaleLine(),
          /* new OpenLayers.Control.MousePosition(), */
          new OpenLayers.Control.LayerSwitcher()
        ],
        projection: epsg900913,
        /* displayProjection: epsg4326 //Is used for displaying coordinates in appropriate CRS by MousePosition control */
      });

      // Define requested (usually Europe-level) overlay; Load from .geojson file.
      // Note that the content is not loaded immediately, so it is unavailable at this moment!
      geojson_protocol = new OpenLayers.Protocol.HTTP({
        url: settings.indicators_query.geo_url,
        format: new OpenLayers.Format.GeoJSON(),
      });

      // /////////////////////////////////////////////////////////////////////
      //
      // Define Layers to be used on the map object.
      //
      var arrLayers = new Array();

      // Empty base.
      arrLayers.Base = new OpenLayers.Layer.Vector("Clear", {isBaseLayer: true});

      // Google OpenStreetMap layer.
      if (settings.indicators_query.geo_use_google_osm) {
        arrLayers.GoogleOSM = new OpenLayers.Layer.Google('Google Map');
      }

      // Google Satellite layer.
      if (settings.indicators_query.geo_use_google_satellite) {
        arrLayers.GoogleSatellite = new OpenLayers.Layer.Google('Google Satellite', {
          type: google.maps.MapTypeId.SATELLITE, /* Available types: ROADMAP, SATELLITE, HYBRID, TERRAIN */
          sphericalMercator: true
        });
      }

      // Google Terrain layer.
      if (settings.indicators_query.geo_use_google_terrain) {
        arrLayers.GoogleTerrain = new OpenLayers.Layer.Google('Google Terrain', {
          type: google.maps.MapTypeId.TERRAIN, /* Available types: ROADMAP, SATELLITE, HYBRID, TERRAIN */
          sphericalMercator: true
        });
      }

      // Google RoadMap layer.
      if (settings.indicators_query.geo_use_google_roadmap) {
        arrLayers.GoogleRoadmap = new OpenLayers.Layer.Google('Google Roadmap', {
          type: google.maps.MapTypeId.ROADMAP,
          sphericalMercator: true
        });
      }

      // Google Hybrid layer.
      if (settings.indicators_query.geo_use_google_hybrid) {
        arrLayers.GoogleHybrid = new OpenLayers.Layer.Google('Google Hybrid', {
          type: google.maps.MapTypeId.HYBRID,
          sphericalMercator: true
        });
      }

      // Define the requested layer by the indicator module (like Europe).
      arrLayers.IndicatorLayer  = new OpenLayers.Layer.Vector("Europe", {
        projection: epsg4326,
        strategies: [new OpenLayers.Strategy.Fixed()],
        protocol: geojson_protocol,
        /*
        styleMap: new OpenLayers.StyleMap({
          'title': "${NUTS_ID}"
        }),
         */
      });

      //
      // /////////////////////////////////////////////////////////////////////
      //
      // /////////////////////////////////////////////////////////////////////
      // Loading indicator + loadstart / loadend events.
      // When load ends for Europe layer, we load in arrays
      // (for later show/hide process) and initially remove all NUTS1-3 polygons/features.
      //
      var LAYERS_LOADING = 0;
      // Move loading div to map div.
      jQuery('#map').append(jQuery('#ol_loading'));

      arrLayers.IndicatorLayer.events.register('loadstart', this, function () {
        LAYERS_LOADING++;
        document.getElementById('ol_loading').style.display = 'block';
      });

      var cancelme = arrLayers.IndicatorLayer.events.register('loadend', this, function () {

        LAYERS_LOADING--;

        if (LAYERS_LOADING == 0) {
          document.getElementById('ol_loading').style.display = 'none';
          arrLayers.IndicatorLayer.events.unregister(cancelme);

          // Load all NUTS1-3 polygons into arrays and initially remove them
          // from the layer map (we only want NUTS0= countries).
          layer_total_features = arrLayers.IndicatorLayer.features.length;

          for (var i = 0; i < layer_total_features; i++) {

            if (arrLayers.IndicatorLayer.features[i]) {

              if (arrLayers.IndicatorLayer.features[i].data.STAT_LEVL_ == 0) {
                // It does add the title to the PATH object, but not showing up.
                jQuery("#" + arrLayers.IndicatorLayer.features[i].geometry.id).attr("title", "test");

                tmp_substr = arrLayers.IndicatorLayer.features[i].data.NUTS_ID.substr(0,2);
                arrLX[0][tmp_substr] = arrLayers.IndicatorLayer.features[i];
              }
              else if (arrLayers.IndicatorLayer.features[i].data.STAT_LEVL_ == 1) {
                tmp_substr = arrLayers.IndicatorLayer.features[i].data.NUTS_ID.substr(0,2);
                if (!(tmp_substr in arrLX[1])) {
                  arrLX[1][tmp_substr] = new Array();
                }
                arrLX[1][tmp_substr].push(arrLayers.IndicatorLayer.features[i]);
              }
              else if (arrLayers.IndicatorLayer.features[i].data.STAT_LEVL_ == 2) {
                tmp_substr = arrLayers.IndicatorLayer.features[i].data.NUTS_ID.substr(0,2);
                if (!(tmp_substr in arrLX[2])) {
                  arrLX[2][tmp_substr] = new Array();
                }
                arrLX[2][tmp_substr].push(arrLayers.IndicatorLayer.features[i]);
              }
              else if (arrLayers.IndicatorLayer.features[i].data.STAT_LEVL_ == 3) {
                tmp_substr = arrLayers.IndicatorLayer.features[i].data.NUTS_ID.substr(0,2);
                if (!(tmp_substr in arrLX[3])) {
                  arrLX[3][tmp_substr] = new Array();
                }
                arrLX[3][tmp_substr].push(arrLayers.IndicatorLayer.features[i]);
              }

            }

          }

          /* if (max_nuts_lvl >= 1) { */
          for (var idx in arrLX[1]) {
            arrLayers.IndicatorLayer.removeFeatures(arrLX[1][idx]);
          }
          /* } */
          /* if (max_nuts_lvl >= 2) { */
          for (var idx in arrLX[2]) {
            arrLayers.IndicatorLayer.removeFeatures(arrLX[2][idx]);
          }
          /* } */
          /* if (max_nuts_lvl >= 3) { */
          for (var idx in arrLX[3]) {
            arrLayers.IndicatorLayer.removeFeatures(arrLX[3][idx]);
          }
          /* } */
          //
          // When we are on GET with country, from the URL, select that country on map;
          // this is done by getting the country dropdown value because it changes with the GET param as well.
          //
          // 'geo_id' is not empty.
          if (settings.indicators_query.geo_id != '') {
            add_selected_region(jQuery('#' + settings.indicators_query.geo_id).val());
            selected_geo = arrLayers.IndicatorLayer.getFeaturesByAttribute('NUTS_ID', jQuery('#' + settings.indicators_query.geo_id).val());
            if (selected_geo.length > 0) {
              selectCtrl.unselectAll();
              selectCtrl.select(selected_geo[0]);
            }
          }

        } // if (LAYERS_LOADING == 0)

        // End of arrLayers.IndicatorLayer.events.register function.
      });

      //
      // /////////////////////////////////////////////////////////////////////
      //
      // /////////////////////////////////////////////////////////////////////
      //
      //  Callback functions for the shapes/polygons on various events.
      //
      //
      // When clicking on a country (polygon), that country NUTS level goes one level up,
      // by loading the next NUTS polygons/features from arrLX[1],2,3.
      // The map is centered and zoomed on it.
      var onClick = function(poly) {

        // First of all we check if it's click on a NUTS0 polygon = country.
        if (poly.data.STAT_LEVL_ == 0) {

          // Now we check if there was any other selected country
          // so we can remove that countries NUTS1-3 polygons.
          if (curr_nuts0_id != '') {

            if (curr_nuts_lvl >= 1) {
              arrLayers.IndicatorLayer.removeFeatures(arrLX[1][curr_nuts0_id]);
            }
            if (curr_nuts_lvl >= 2) {
              arrLayers.IndicatorLayer.removeFeatures(arrLX[2][curr_nuts0_id]);
            }
            if (curr_nuts_lvl >= 3) {
              arrLayers.IndicatorLayer.removeFeatures(arrLX[3][curr_nuts0_id]);
            }

          }

          // We load polygons and set as current for the clicked country, for NUTS1 (next level).
          curr_nuts0_id = poly.data.NUTS_ID;
          if (settings.indicators_query.max_nuts_lvl > 0) {
            curr_nuts_lvl = 1;
            arrLayers.IndicatorLayer.addFeatures(arrLX[1][curr_nuts0_id]);
          }

          jQuery('#curr_country').html(curr_nuts0_id);
          jQuery('#curr_country_ctrl').show();

          jQuery('#curr_nuts').html(curr_nuts_lvl);
          jQuery('#curr_nuts_ctrl').show();

        }

        // We check the level of NUTS.
        // We load the next NUTS level only if current level < max_nuts_lvl.
        else if (curr_nuts_lvl < settings.indicators_query.max_nuts_lvl) {
          curr_nuts_lvl++;
          if (curr_nuts_lvl == 2 && curr_nuts0_id in arrLX[2]) {
            arrLayers.IndicatorLayer.addFeatures(arrLX[2][curr_nuts0_id]);
            jQuery('#curr_nuts').html(curr_nuts_lvl);
          }
          if (curr_nuts_lvl == 3 && curr_nuts0_id in arrLX[3]) {
            arrLayers.IndicatorLayer.addFeatures(arrLX[3][curr_nuts0_id]);
            jQuery('#curr_nuts').html(curr_nuts_lvl);
          }
        }

        // Update the indicators dropdown as well, propagate the event
        // 'geo_id' is empty. Redirect to 'path'
        if (settings.indicators_query.geo_id == '') {
          redirect_to = settings.indicators_query.path;
          Drupal.indicators_query.popup_country(poly.data.NUTS_ID);
        }
        // Update geo_id
        else {
          // Display the country to fit the screen.
          map.zoomToExtent(poly.geometry.getBounds());
          // Recenter display on the country.
          map.setCenter(poly.geometry.getCentroid().transform(map.projection, map.displayProjection));

          jQuery('#' + settings.indicators_query.geo_id).val(poly.data.NUTS_ID).change();
        }
      } // function()

      //
      // /////////////////////////////////////////////////////////////////////
      //
      // /////////////////////////////////////////////////////////////////////
      //
      //  Controls to be attached to the layers.
      //
      //
      // Highlight Control.
      // Show region name on hover.
      var showRealName = function(e) {
        // ("Event"+e.type+" "+e.feature.id);
        jQuery('#region_tooltip').html(arr_regions_info[e.feature.data.NUTS_ID]);
      };

      var hideRealName = function(e) {
        jQuery('#region_tooltip').html(' ');
      };

      var highlightCtrl = new OpenLayers.Control.SelectFeature(arrLayers.IndicatorLayer, {
        hover: true,
        highlightOnly: true,
        renderIntent: "temporary",
        eventListeners: {
          beforefeaturehighlighted: showRealName,
          // featurehighlighted: report,
          featureunhighlighted: hideRealName
        }
        /*
        overFeature: function(feature) {
          //
        },
         */

      });

      map.addControl(highlightCtrl);
      highlightCtrl.activate();

      // Select Control.
      var selectCtrl = new OpenLayers.Control.SelectFeature(arrLayers.IndicatorLayer, {
        clickout: true,
        onSelect: onClick,
        renderIntent: "select",
      });

      map.addControl(selectCtrl);
      selectCtrl.activate();

      //
      // /////////////////////////////////////////////////////////////////////
      //
      // Add base layers and overlay options to the map layers control on the right.
      // Add Google OSM if requested.
      if (settings.indicators_query.geo_use_google_osm) {
        map.addLayers([arrLayers.GoogleOSM]);
      }
      // Add Google Satellite if requested.
      if (settings.indicators_query.geo_use_google_satellite) {
        map.addLayers([arrLayers.GoogleSatellite]);
      }
      // Add Google Terrain if requested.
      if (settings.indicators_query.geo_use_google_terrain) {
        map.addLayers([arrLayers.GoogleTerrain]);
      }
      // Add Google Roadmap if requested.
      if (settings.indicators_query.geo_use_google_roadmap) {
        map.addLayers([arrLayers.GoogleRoadmap]);
      }
      // Add Google Hybrid if requested.
      if (settings.indicators_query.geo_use_google_hybrid) {
        map.addLayers([arrLayers.GoogleHybrid]);
      }

      map.addLayers(
        [
          arrLayers.IndicatorLayer
        ]
      );

      // Google OSM has to be the active base, for a better map view and countries recognition
      // (especially when choosing countries with out-Europe territories).
      map.setBaseLayer(arrLayers.GoogleOSM);

      // Render and display layers - Set map's center and zoom level.
      map.setCenter(new OpenLayers.LonLat(settings.indicators_query.geo_lon, settings.indicators_query.geo_lat).transform(epsg4326, epsg900913), settings.indicators_query.geo_zoom);

      // We move the current country+NUTS level div to the blue dataLayersDiv.
      jQuery('.dataLayersDiv').append(jQuery('#curr_ctrl'));
      jQuery('#curr_country_ctrl').click(function() {clear_curr_country();});
      jQuery('#curr_nuts_ctrl').click(function() {back_nuts1();});

      function clear_curr_country() {

        // Clear current country NUTS levels.
        if (curr_nuts_lvl >= 1) {
          arrLayers.IndicatorLayer.removeFeatures(arrLX[1][curr_nuts0_id]);
        }
        if (curr_nuts_lvl >= 2) {
          arrLayers.IndicatorLayer.removeFeatures(arrLX[2][curr_nuts0_id]);
        }
        if (curr_nuts_lvl >= 3) {
          arrLayers.IndicatorLayer.removeFeatures(arrLX[3][curr_nuts0_id]);
        }

        // Reset vars.
        curr_nuts0_id = '';
        curr_nuts_lvl = 0;

        // Hide current country+NUTS
        jQuery('#curr_country_ctrl').hide();
        jQuery('#curr_nuts_ctrl').hide();

        // Recenter the map on Europe.
        map.setCenter(new OpenLayers.LonLat(settings.indicators_query.geo_lon, settings.indicators_query.geo_lat).transform(epsg4326, epsg900913), settings.indicators_query.geo_zoom);

      }

      function back_nuts1() {

        // Clear current country NUTS levels if > 1
        if (curr_nuts_lvl >= 2) {
          arrLayers.IndicatorLayer.removeFeatures(arrLX[2][curr_nuts0_id]);
        }
        if (curr_nuts_lvl >= 3) {
          arrLayers.IndicatorLayer.removeFeatures(arrLX[3][curr_nuts0_id]);
        }

        // Reset vars.
        curr_nuts_lvl = 1;
        jQuery('#curr_nuts').html(curr_nuts_lvl);

        // Recenter the map on that country.
        map.zoomToExtent(arrLX[0][curr_nuts0_id].geometry.bounds);

      }

      // 'geo_id' is not empty.
      if (settings.indicators_query.geo_id != '') {
        // EVENT: Input geo field changed ==> Display new region on the map.
        jQuery('#' + settings.indicators_query.geo_id).change(function() {
          add_selected_region(jQuery(this).val());

          selected_geo = arrLayers.IndicatorLayer.getFeaturesByAttribute('NUTS_ID', jQuery(this).val());
          if (selected_geo.length > 0) {
            // map.zoomToExtent(selected_geo[0].geometry.bounds);
            selectCtrl.unselectAll();
            selectCtrl.select(selected_geo[0]);
          }
        });
      }

      // Add a NUTS1..3 region to the Layers, so it can be displayed.
      function add_selected_region(nuts_code) {
        // e.g 'BE', 'BE3', 'BE32' ...
        curr_nuts0_id = nuts_code.toUpperCase();
        curr_nuts_lvl = curr_nuts0_id.length - 2;
        curr_nuts0_id0 = curr_nuts0_id.substring(0,2);
        // Find NUTS code in one of the tables and add it to the layers.
        if (curr_nuts_lvl >= 1 && curr_nuts_lvl <= 3 && curr_nuts0_id0 in arrLX[curr_nuts_lvl]) {
          // Find NUTS in the array.
          for (i = 0; i < arrLX[curr_nuts_lvl][curr_nuts0_id0].length; i++) {
            if (arrLX[curr_nuts_lvl][curr_nuts0_id0][i].data.NUTS_ID == curr_nuts0_id) {
              arrLayers.IndicatorLayer.addFeatures(arrLX[curr_nuts_lvl][curr_nuts0_id0][i]);
              jQuery('#curr_nuts').html(curr_nuts_lvl);
            }
          }
        }
      }

    }
  };

})(jQuery);
