<?php
/**
 * @file
 * Import function for StatPlanet Indicators Import (datasets).
 *
 * @see http://www.statsilk.com/software/statplanet
 */

/**
 * Save data to SQL table using "data" module, for StatPlanet format.
 *
 * @param object $table
 *   SQL table to insert data to.
 * @param array $lines
 *   Array of arrays of records to process; it comes from CSV file.
 * @param int $l
 *   Starting line number.
 * @param array $settings
 *   Indicator tid, fields, filters, vocabularies.
 * @param int $skipped
 *   Number of skipped (filtered out) records.
 *
 * @return int
 *   Number of records written to the table.
 */
function _indicators_import_data_save_line_statplanet(&$table, array &$lines, &$l, array &$settings, &$skipped) {
  static $var_names;
  static $var_values;
  static $written;  /* Number of written records. */
  static $missing;  /* Missing vocabulary codes, to suppress duplicate error messages. */
  static $data_fields;
  static $delta_line;
  static $errors;
  static $indicator_parent;

  // Get list of fields of the data table.
  if (empty($data_fields)) {
    $data_fields = array_keys(data_get_field_definitions());
  }

  // Process batch lines.
  foreach ($lines as $batch => $line) {

    // Calculate data records out of imported line.
    $data = array();

    // First line has 11 labels and then geo codes (ISO-3):
    // CATEGORY,TIME,INDICATOR,SOURCE,DESCRIPTION,UNIT,MAP,GRAPH,
    // FILE,OPTIONS,TYPE,...
    if ($batch == 0 && $l == 0) {
      $written = 0;

      // Process variable names.
      foreach ($line as $v => $name) {
        $var_names[$name] = $v;
      }
      // Variable names are saved in static var for later use.
      // Load geo_iso3 vocabulary (Three-character country codes).
      $voc = taxonomy_vocabulary_machine_name_load('geo_iso3');
      $voc_tree = taxonomy_get_tree($voc->vid, 0, NULL, TRUE);
      $settings['vocabularies']['geo_iso3'] = array();
      foreach ($voc_tree as $term) {
        if (!empty($term->field_es_code[LANGUAGE_NONE][0]['value'])) {
          $settings['vocabularies']['geo_iso3'][$term->name] = $term->field_es_code[LANGUAGE_NONE][0]['value'];
        }
      }

      continue;
    }
    // Next lines have data.
    else {
      foreach ($line as $r => $row) {
        // First 11 rows have variables.
        if ($r < 12) {
          if ($r == $var_names['CATEGORY']) {
            if (!empty($row)) {
              // Add value to INDICATOR vocabulary.
              // It will be the parent of "indicators" (see code 40 line below)
              // [0]='taxonomy, [1]=sp_indicator'
              $field_type = explode(':', $settings['fields']['indicator']);
              $voc_machine_name = $field_type[1];
              $voc_name = empty($field_type[2]) ? $field_type[1] : $field_type[2];
              $term = _indicators_create_statplanet_term($voc_machine_name, $voc_name, $row, $row, $row, 0);
              $settings['vocabularies'][$voc_machine_name][$row] = $term->tid;
              $indicator_parent = $term->tid;
              // Save for later.
              $var_values[$r] = $row;
            }
            // Get saved value.
            $vars[$r] = $var_values[$r];
          }
          elseif ($r == $var_names['TIME']) {
            if (!empty($row)) {
              // Save for later.
              $var_values[$r] = $row;
              $delta_line = 0;
            }
            else {
              ++$delta_line;
            }
            // Get saved value.
            $vars[$r] = $var_values[$r];
          }
          elseif ($r == $var_names['INDICATOR']) {
            if (!empty($row)) {
              // Special value, means "Idem".
              if ($row == '-') {
              }
              else {
                // Remove leading header characters,
                // for example "AMT - Market share in patent activities".
                $code_array = explode(' - ', $row);
                if (isset($code_array[1])) {
                  // "Market share in patent activities".
                  $row = trim($code_array[1]);
                }
                // Save for later.
                $var_values[$r][$delta_line] = $row;
              }
            }
            if (isset($var_values[$r][$delta_line])) {
              // Get saved value.
              $vars[$r] = $var_values[$r][$delta_line];
            }
          }
          elseif ($r == $var_names['SOURCE']) {
            if (!empty($row)) {
              // Save for later.
              $var_values[$r][$delta_line] = $row;
            }
          }
          elseif ($r == $var_names['DESCRIPTION']) {
            if (!empty($row)) {
              // Add value to INDICATOR vocabulary.
              // [0]='taxonomy, [1]=sp_indicator'
              $field_type = explode(':', $settings['fields']['indicator']);
              $voc_machine_name = $field_type[1];
              $voc_name = empty($field_type[2]) ? $field_type[1] : $field_type[2];
              $code = $var_values[$var_names['INDICATOR']][$delta_line];
              // Remove leading header characters,
              // for example "AMT - Market share in patent activities".
              $code_array = explode(' - ', $code);
              if (isset($code_array[1])) {
                // "Market share in patent activities".
                $code = trim($code_array[1]);
              }
              $term = _indicators_create_statplanet_term($voc_machine_name, $voc_name, $code, $code, $code, $indicator_parent);
              $settings['vocabularies'][$voc_machine_name][$code] = $term->tid;
              // Save for later.
              $var_values[$r][$delta_line] = $row;
            }
          }
          elseif ($r == $var_names['UNIT']) {
            $code = $row;
            // It's clear...
            if ($row == '%') {
              $row = 'Percentage';
              $code = 'PC';
            }
            // It's a Number if the Indicator is empty
            // or has '-' value meaning "same" as $delta_line lines ago.
            else {
              if (!empty($line[$var_names['INDICATOR']]) && $line[$var_names['INDICATOR']] != '-') {
                $row = 'Number';
                $code = 'NBR';
              }
              // Its value should have been stored previously.
              elseif (isset($var_values[$r][$delta_line])) {
                $row = $var_values[$r][$delta_line];
                $code = ($row == 'Number') ? 'NBR' : 'PC';
              }
              else {
              }
            }
            if (!empty($row)) {
              // Add value to UNIT vocabulary.
              // [0]='taxonomy, [1]=sp_unit'.
              $field_type = explode(':', $settings['fields']['unit']);
              $voc_machine_name = $field_type[1];
              $voc_name = empty($field_type[2]) ? $field_type[1] : $field_type[2];
              if (!isset($settings['vocabularies'][$voc_machine_name][$row])) {
                $term = _indicators_create_statplanet_term($voc_machine_name, $voc_name, $code, $row, $code);
                $settings['vocabularies'][$voc_machine_name][$row] = $term->tid;
              }
              // Save for later.
              $var_values[$r][$delta_line] = $row;
            }
            if (isset($var_values[$r][$delta_line])) {
              // Get saved value.
              $vars[$r] = $var_values[$r][$delta_line];
            }
          }
          elseif ($r == $var_names['MAP']) {
            // Ignore.
          }
          elseif ($r == $var_names['GRAPH']) {
            // Ignore.
          }
          elseif ($r == $var_names['FILE']) {
            // Ignore.
          }
          elseif ($r == $var_names['OPTIONS']) {
            // Ignore.
          }
          elseif ($r == $var_names['TYPE']) {
            // Ignore.
          }
        }
        // The row 12 and after have values.
        else {
          if ($row != NULL) {
            // Check if the record is filtered out.
            if (_indicators_import_filter_out($vars, $var_names, $settings['filters'])) {
              ++$skipped;
              // Skip the record.
              continue;
            }
            // Get 3-char and 2-char geo codes.
            $geo_code3 = array_search($r, $var_names);
            $geo_code = $settings['vocabularies']['geo_iso3'][$geo_code3];
            if (!isset($settings['vocabularies'][INDICATORS_VOC_GEO][$geo_code])) {
              if (!isset($missing[INDICATORS_VOC_GEO][$geo_code3])) {
                drupal_set_message(t('Taxonomy term for the code %name of vocabulary @voc not defined.',
                  array('%name' => $geo_code3, '@voc' => INDICATORS_VOC_GEO)), 'warning');
                $missing[INDICATORS_VOC_GEO][$geo_code3] = TRUE;
              }
              ++$skipped;
              // Skip the record.
              continue;
            }
            // Create a record.
            $record = array(
              'value' => $row,
              'time' => $vars[$var_names['TIME']],
              'flag' => NULL,
              'geo' => $settings['vocabularies'][INDICATORS_VOC_GEO][$geo_code], /* tid of "Geo" term. */
              'source' => $settings['source'], /* tid of "Indicator" term. */
            );
            // Add parsed fields to the record.
            foreach ($var_names as $field_name => $i) {
              $field_name_lower = strtolower($field_name);
              // If the field is not in the fields' list, skip it.
              if (!in_array($field_name_lower, array_keys($settings['fields']))) {
                continue;
              }
              $field_type = $settings['fields'][$field_name_lower];
              // Copy simple data field (varchar, float, etc.)
              if (in_array($field_type, $data_fields)) {
                $record[$field_name_lower] = $vars[$var_names[$field_name]];
              }
              // Calculate taxonomy term's tid.
              elseif (substr($field_type, 0, 8) == 'taxonomy') {
                if (isset($vars[$var_names[$field_name]])) {
                  $voc = substr($field_type, 9);
                  if (isset($settings['vocabularies'][$voc][$vars[$var_names[$field_name]]])) {
                    $record[$field_name_lower] = $settings['vocabularies'][$voc][$vars[$var_names[$field_name]]];
                  }
                  else {
                    // Error about the field has already been displayed?
                    if (empty($errors['term'][$vars[$var_names[$field_name]]])) {
                      drupal_set_message(t('Taxonomy term %name of vocabulary @voc not defined.',
                        array('%name' => $vars[$var_names[$field_name]], '@voc' => $voc)), 'warning');
                    }
                    // Avoid duplicate error messages.
                    $errors['term'][$vars[$var_names[$field_name]]] = TRUE;
                  }
                }
                else {
                  // Error about the field has already been displayed?
                  if (empty($errors['field'][$field_name])) {
                    drupal_set_message(t('Field %name not found in the source file. Please check the Source fields of the Indicator.',
                      array('%name' => $field_name)), 'warning');
                  }
                  // Avoid duplicate error messages.
                  $errors['field'][$field_name] = TRUE;
                }
              }
              else {
                drupal_set_message(t('Unsupported field (yet): @name. Import aborted.',
                  array('@name' => $field_name_lower)), 'error');
                return 0;
              }
            }
            // Add the record to the result.
            $data[] = $record;
          }
        }
      }
    }

    if (!empty($data)) {
      // Create an "INSERT TO" query for multiple records,
      // which will run much faster!
      $sql = 'INSERT INTO {' . $table->name . '} (' . implode(', ', array_keys($data[0])) . ') VALUES ';
      foreach ($data as $record) {
        $rec = '(';
        foreach ($record as $value) {
          $rec .= ($value == '' ? "NULL, " : "'$value', ");
        }
        $sql .= substr($rec, 0, -2) . '), ';
      }
      $sql = substr($sql, 0, -2) . ';';
      db_query($sql);
      $written += count($data);
    }
    ++$l;
  }

  // Number of written records.
  return $written;
}

/**
 * HELPER FUNCTION: Create a taxonomy term in a vocabulary.
 *
 * @param string $voc_machine_name
 *   Vocabulary info.
 * @param string $voc_name
 *   Vocabulary info.
 * @param string $code
 *   Term info.
 * @param string $name
 *   Term info.
 * @param string $description
 *   Term info.
 * @param int $parent
 *   Term tid of the parent term.
 *
 * @return object
 *   term object created in Dictionaries vocabulary
 */
function _indicators_create_statplanet_term($voc_machine_name, $voc_name, $code, $name, $description = '', $parent = NULL) {
  $vocabulary = taxonomy_vocabulary_machine_name_load($voc_machine_name);

  if (!$vocabulary) {
    // Create a new vocabulary.
    $vocabulary = new stdClass();
    $vocabulary->name = $voc_name;
    $vocabulary->machine_name = $voc_machine_name;
    $vocabulary->description = '';
    taxonomy_vocabulary_save($vocabulary);
    // Attach field instances.
    $field_instances = _indicators_statplanet_field_instances($voc_machine_name);
    foreach ($field_instances as $instance) {
      field_create_instance($instance);
    }
    // Inform.
    drupal_set_message(t('Vocabulary %name created.', array('%name' => $vocabulary->name)));
  }

  // If term exists already.
  $tid = _indicators_term_find_code($voc_machine_name, $code);
  if ($tid) {
    // Load term.
    $term = taxonomy_term_load($tid);
  }
  else {
    // Create term.
    $term = new stdClass();
    $term->vid = $vocabulary->vid;
    $term->language = LANGUAGE_NONE;
    $term->format = 'plain_text';
    $term->name = $name;
    $term->description = $description;
    $term->field_es_code[LANGUAGE_NONE][0] = array(
      'value' => strtoupper($code),
      'format' => NULL,
      'safe_value' => strtoupper($code),
    );
    $term->parent = isset($parent) ? $parent : 0;
    taxonomy_term_save($term);
    // Create an entry for translation.
    $t = get_t();
    $t($description);
  }
  return $term;
}

/**
 * List of field instances for Indicator Vocabulary.
 *
 * @param string $bundle
 *   Taxonomy machine name.
 */
function _indicators_statplanet_field_instances($bundle) {
  $field_instances = array();

  // Exported field_instance: 'taxonomy_term-$bundle-field_es_code'
  $field_instances['taxonomy_term-' . $bundle . '-field_es_code'] = array(
    'bundle' => $bundle,
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Code of the ' . substr($bundle, 3) . '.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 1,
      ),
    ),
    'entity_type' => 'taxonomy_term',
    'field_name' => 'field_es_code',
    'label' => 'Code',
    'required' => 1,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 33,
    ),
  );

  return $field_instances;
}
