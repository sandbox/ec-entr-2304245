INTRODUCTION
------------

Indicators module imports statistical data.
It is intended for use by European Commission Drupal community.

Features include:
 * Automatically import statistical data from remote CSV files
 * Support of input data formats:
   - in Eurostat (http://ec.europa.eu/eurostat)
     Import Eurostat raw statistical datasets ("Indicators") to SQL table(s)
     (defined by Data module).
     Import Eurostat dictionaries into Drupal vocabularies ("Dictionaries")
   - StatPlanet Data Editor (https://sourceforge.net/projects/statplaneteditr)

REQUIREMENTS
------------

This module requires the following modules:
 
 * Data (https://www.drupal.org/project/data)
 * Features (https://www.drupal.org/project/features)
 * i18n (https://www.drupal.org/project/i18n) 
 * Indicators (https://www.drupal.org/sandbox/ec-entr/2304245)

INSTALLATION
------------

 * Place Indicators Import module into your modules directory.
   This is normally the "sites/all/modules/custom" directory.

 * Go to admin/modules. Enable the modules.
   The Indicators modules is found in the ENTR section.

CONFIGURATION
-------------

 * Setup the module in admin/config/entr/indicators

 * Data structure

   An Indicator is defined as a term in 'Indicator' vocabulary, with a
   corresponding Data Table (Data module). Data module simply describes fields
   in an SQL table and provides an API to manage tables and records. Some fields
   can only have values from a Dictionary (e.g. Age, Unit).

   All Dictionaries used in an Indicator must first be defined, and their
   content imported, before an Indicator's dataset can be imported.

   When you activate the module, it will automatically create the following
   Dictionaries:
   - Eurostat Indicator
   - Eurostat Dictionary
   - Eurostat Flags
   - Eurostat Units
   - Eurostat Geo codes

   The detailed manual for developers is in development.
   Visit the project page on Drupal.org:

   https://www.drupal.org/sandbox/ec-entr/2304245

 * Define indicator terms corresponding to input files
   
   - You are able to organize your Indicators in a classical taxonomy tree way.
     It should not exceed 2 levels of depth though. In order to create branches,
     don't fill any field except the title and description (optional).
     To create a leaf, fill in all the necessary information, for example:

     Code             DEMO_R_D2JAN
     Name             Population Density
     Decsription      Population on 1 January (Total) - NUTS 2 regions
     Authored on      clear the field
     Unit             Number
     Data table       rim_data_sheet
     Data table title RIM Data sheet
     Source list url  [1]/BulkDownloadListing?dir=data&sort=1&sort=2&start=d
     Meta data url    [2]/demo_pop_esms.htm
     Source url       [1]/BulkDownloadListing?file=data%2Fdemo_r_d2jan.tsv.gz
     Source file name demo_r_d2jan.tsv.gz
     Source fields    finish=varchar:Time
                      partner=taxonomy:es_geo:Geo codes
                      unit=taxonomy:es_unit:Unit codes
     Source filters   sex=T
                      age=TOTAL

   - When you run import of an Indicator, the module will automatically create a
     Data table with these fields:
     - eid       ID of the data record
     - geo       The tid of the Geo term corresponding to geo code
     - value     Record's value
     - flag      The tid of the Flag term if the record's value is flagged
     - time      A string usually corresponding to the record's Year
     - source    The tid of the corresponding Indicator term. This lets us
                 store multiple Indicators into one single SQL Data table

     It will also create any additional Source fields.
     The format of a Source field: {field_name}={field_type}:[{field_title}]

     Notice that when some of the additional fields define a dictionary, the
     corresponding Taxonomy will be created but not populated. You will need to
     define the corresponding entries in the Dictionary taxonomy and run Import
     on them before running an import on the Indicator again.

     Notice that the Data table name (machine name of an SQL table) will need
     to be chosen carefully and should follow some standard.

     A hint: if you experience a PHP timeout while importing a large dataset,
     try to split it in two or more parts by using appropriate filters

   - Create the Dictionaries

     For all the Dictionaries which are not automatically created by the module
     at the installation time, you must add a corresponding entry into
     Dictionary vocabulary and run import, for example:

     Code              AGE
     Name              Age
     Decsription       People's age
     Source list url   [1]/BulkDownloadListing?sort=1&dir=dic%2Fen
     Source url        [1]/BulkDownloadListing?sort=1&file=dic%2Fen%2Fage.dic
     Source file name  age.dic
     Taxonomy name     es_age

     You are able to organize your Dictionary vocabulary in a classical taxonomy
     tree way. It should not exceed 2 levels of depth though. You can also
     reorganize terms in the imported dictionary by changing their weights or tree
     structure, without impact by any future imports.
   
   - Copy files from indicators/indicators_import/templates directory
     to your ADMINISTRATION theme's (rubik for example) templates directory.
 
 * Import statistical data from in admin/config/entr/import
   
   If the previous steps were done properly, you should see an organized list of
   all datasets to import.

   - Import all necessary Eurostat dictionaries first: admin/config/entr/dic

   - You can now import the Indicator datasets: admin/config/entr/import

     Import one dataset at a time and check any error message (for example
     missing codes in a dictionary). Correct them by importing or adding terms
     manually and run the import again if necessary.

   - Check imported data in admin/structure/data

 * Upgrading
  
   When a statistical data file is updated on Eurostat file, it gets 
   automatically imported via a CRON job. A notification email is then sent.
   In case the structure of the file is marked as changed, the import must be
   done manually in admin/config/entr/import

MAINTAINERS
-----------

  * EC-GROW - https://www.drupal.org/u/ec-grow
    (GROW-DRUPAL-ORG@ec.europa.eu)

  * Rouslan Sorokine - https://www.drupal.org/u/rsorokine
  * Thibaut Van Bellinghen - https://www.drupal.org/u/tvb1507