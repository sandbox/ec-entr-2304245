<?php
/**
 * @file
 * Features file: indicators_query.features.taxonomy.inc.
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function indicators_query_taxonomy_default_vocabularies() {
  return array(
    'es_query' => array(
      'name' => 'Eurostat Queries and Displays',
      'machine_name' => 'es_query',
      'description' => 'Queries and Displays of Eurostat statistical data',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
      'language' => 'und',
      'i18n_mode' => 1,
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
