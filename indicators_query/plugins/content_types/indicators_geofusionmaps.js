/**
 * @file
 * Indicators FusionMaps JS implementation.
 *
 * Coded on 2014-05-16.
 *
 * Display a FusionMap map with its controls.
 * When user clicks on a region, the user is redirected to another url
 */

// This function is called when html body is loaded.
function indicators_geo_init()
{
}

// Base path where to go.
var redirect_to;


(function($) {

  /**
   * OnClick event.
   */
  Drupal.indicators_query = {

    // The name of this function must match that contained in Map descriptor file.
    popup_country : function(country_code, settings) {
      window.location = redirect_to + country_code;
    },

  }
})(jQuery);


(function($) {

  /**
   * Display FusionMap.
   */
  Drupal.behaviors.indicators_query = {
    // Settings:
    // - 'geo_id' DOM id of the option list to update on a click. Not used. Redirect to 'path' is forced.
    // - 'map'  Map file
    // - 'data' Map descriptor and display parameters file
    // - 'path' url (absolute or internal) where to go when user clicks on a region. Region code will be appended to it.
    attach: function (context, settings) {
      var map_width = document.getElementById('mapdiv').offsetWidth;
      var map_height = Math.round(map_width * 100 / 100);
      var map = new FusionCharts(settings.indicators_query.map, "MapId", map_width, map_height, "0", "0");
      redirect_to = settings.indicators_query.path;
      map.setXMLUrl(settings.indicators_query.data);
      map.render("mapdiv");
    }
  };

})(jQuery);
