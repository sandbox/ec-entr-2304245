<?php
/**
 * @file
 * Features file: indicators_query.features.field_instance.inc.
 */

/**
 * Implements hook_field_default_field_instances().
 */
function indicators_query_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'taxonomy_term-es_query-field_es_code'
  $field_instances['taxonomy_term-es_query-field_es_code'] = array(
    'bundle' => 'es_query',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(
          'masonry' => FALSE,
          'masonry_animated' => 0,
          'masonry_animation_duration' => 500,
          'masonry_column_width' => '',
          'masonry_column_width_units' => 'px',
          'masonry_fit_width' => 0,
          'masonry_gutter_width' => 0,
          'masonry_images_first' => 1,
          'masonry_resizable' => 1,
          'masonry_rtl' => 0,
        ),
        'type' => 'text_default',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'taxonomy_term',
    'field_name' => 'field_es_code',
    'label' => 'Code',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 2,
    ),
  );

  // Exported field_instance: 'taxonomy_term-es_query-field_es_dimensions'
  $field_instances['taxonomy_term-es_query-field_es_dimensions'] = array(
    'bundle' => 'es_query',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Enter the maximum number of dimensions of the resulting table a query can return. It is usually equal to the number of fields of the data table.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'number',
        'settings' => array(
          'decimal_separator' => '.',
          'prefix_suffix' => TRUE,
          'scale' => 0,
          'thousand_separator' => ' ',
        ),
        'type' => 'number_integer',
        'weight' => 3,
      ),
    ),
    'entity_type' => 'taxonomy_term',
    'field_name' => 'field_es_dimensions',
    'label' => 'Dimensions',
    'required' => 0,
    'settings' => array(
      'max' => '',
      'min' => 0,
      'prefix' => '',
      'suffix' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'number',
      'settings' => array(),
      'type' => 'number',
      'weight' => 5,
    ),
  );

  // Exported field_instance: 'taxonomy_term-es_query-field_es_flat_data'
  $field_instances['taxonomy_term-es_query-field_es_flat_data'] = array(
    'bundle' => 'es_query',
    'default_value' => array(
      0 => array(
        'value' => 0,
      ),
    ),
    'deleted' => 0,
    'description' => 'Some indicators may have a single value for one year, which applies to any year, like a country\'s surface.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 5,
      ),
    ),
    'entity_type' => 'taxonomy_term',
    'field_name' => 'field_es_flat_data',
    'label' => 'Flat data',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'display_label' => 1,
      ),
      'type' => 'options_onoff',
      'weight' => 7,
    ),
  );

  // Exported field_instance: 'taxonomy_term-es_query-field_es_indicator'
  $field_instances['taxonomy_term-es_query-field_es_indicator'] = array(
    'bundle' => 'es_query',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Choose one or many sources which may be displayed on one chart. IMPORTANT: All indicators must point to the same data table.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'i18n_taxonomy',
        'settings' => array(),
        'type' => 'i18n_taxonomy_term_reference_link',
        'weight' => 2,
      ),
    ),
    'entity_type' => 'taxonomy_term',
    'field_name' => 'field_es_indicator',
    'label' => 'Indicator',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => 4,
    ),
  );

  // Exported field_instance: 'taxonomy_term-es_query-field_es_max_dimensions'
  $field_instances['taxonomy_term-es_query-field_es_max_dimensions'] = array(
    'bundle' => 'es_query',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Maximum dimensions which can be displayed (usually 2). If the resulting array has more dimensions, it won\'t be displayed.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'number',
        'settings' => array(
          'decimal_separator' => '.',
          'prefix_suffix' => TRUE,
          'scale' => 0,
          'thousand_separator' => ' ',
        ),
        'type' => 'number_integer',
        'weight' => 4,
      ),
    ),
    'entity_type' => 'taxonomy_term',
    'field_name' => 'field_es_max_dimensions',
    'label' => 'Maximum dimensions',
    'required' => 0,
    'settings' => array(
      'max' => 2,
      'min' => 0,
      'prefix' => '',
      'suffix' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'number',
      'settings' => array(),
      'type' => 'number',
      'weight' => 6,
    ),
  );

  // Exported field_instance: 'taxonomy_term-es_query-field_es_source_filters'
  $field_instances['taxonomy_term-es_query-field_es_source_filters'] = array(
    'bundle' => 'es_query',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => "You may filter field values. Use the following format for each entry:\nage=TOTAL\ntime>1999\ngeo=%partner",
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(
          'masonry' => FALSE,
          'masonry_animated' => 0,
          'masonry_animation_duration' => 500,
          'masonry_column_width' => '',
          'masonry_column_width_units' => 'px',
          'masonry_fit_width' => 0,
          'masonry_gutter_width' => 0,
          'masonry_images_first' => 1,
          'masonry_resizable' => 1,
          'masonry_rtl' => 0,
        ),
        'type' => 'text_default',
        'weight' => 1,
      ),
    ),
    'entity_type' => 'taxonomy_term',
    'field_name' => 'field_es_source_filters',
    'label' => 'Query filters',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 3,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Choose one or many sources which may be displayed on one chart. IMPORTANT: All indicators must point to the same data table.');
  t('Code');
  t('Dimensions');
  t('Enter the maximum number of dimensions of the resulting table a query can return. It is usually equal to the number of fields of the data table.');
  t('Flat data');
  t('Indicator');
  t('Maximum dimensions');
  t("Maximum dimensions which can be displayed (usually 2). If the resulting array has more dimensions, it won't be displayed.");
  t('Query filters');
  t("Some indicators may have a single value for one year, which applies to any year, like a country's surface.");
  t("You may filter field values. Use the following format for each entry:\nage=TOTAL\ntime>1999\ngeo=%partner");

  return $field_instances;
}
