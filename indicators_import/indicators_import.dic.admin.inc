<?php

/**
 * @file
 * Admin forms for Indicators Import (dictionaries).
 */

  // Load for common functions.
  module_load_include('inc', 'indicators_import', 'indicators_import.data.admin');

/**
 * Build a form for running a dataset import.
 */
function indicators_import_dic_form($form_values = NULL) {
  // This list source files corresponds to the taxonomy tree 'Dictionary',
  // where only tree leaves correspond to remote files and tree branches
  // are only used to neatly organize those in fieldsets.
  $source_files = _indicators_import_indicators_list(INDICATORS_VOC_DICTIONARY);
  $datasets = array();
  $form = array();
  $options = array();
  $prune = &$form;
  $newer = FALSE;

  // Get info about when indicators have been last imported.
  $updated = variable_get('indicators_import_updated', array());

  // Create form elements: nested fieldsets (2 levels maximum)
  // and options for file downloads.
  foreach ($source_files as $source) {
    // A branch? Create a nested fieldset.
    if (empty($source->field_es_code)) {
      // Do we have any previous options?
      if (!empty($options)) {
        $prune['source_files'] = array(
          '#type' => 'checkboxes',
          '#title' => t('Choose file to import'),
          '#options' => $options,
          '#required' => TRUE,
        );
        $options = array();
      }
      // Level 0 branch?
      if ($source->parents[0] == 0) {
        $form['category_' . $source->tid] = array(
          '#type' => 'fieldset',
          '#title' => check_plain($source->name),
          '#collapsible' => TRUE,
          '#collapsed' => FALSE,
        );
        $prune = &$form['category_' . $source->tid];
      }
      // Level 1 branch.
      else {
        $form['category_' . $source->parents[0]]['category_' . $source->tid] = array(
          '#type' => 'fieldset',
          '#title' => check_plain($source->name),
          '#collapsible' => TRUE,
          '#collapsed' => FALSE,
        );
        $prune = &$form['category_' . $source->parents[0]]['category_' . $source->tid];
      }
    }
    // A leaf? Add to the file options of the last fieldset.
    else {
      // Compile an option line.
      $description = $source->field_es_code[LANGUAGE_NONE][0]['value'];
      $description .= ' - ' . (empty($source->description) ? $source->name : $source->description);
      if (!empty($updated[$source->tid])) {
        $description .= '. ' . t('Imported on @date', array('@date' => date('d/m/Y', $updated[$source->tid])));
      }
      if (!empty($source->field_es_remote_newer[LANGUAGE_NONE][0])) {
        $description .= ' <span class="new">' . t('There is a newer version of this file available') . '</span> : ';
        $description .= '(' . $source->field_es_remote_info[LANGUAGE_NONE][0]['date'];
        $description .= ', ' . $source->field_es_remote_info[LANGUAGE_NONE][0]['size'] . ')';
        $newer = TRUE;
      }
      $description .= ' - ' . l(t('Taxonomy entry'), 'taxonomy/term/' . $source->tid . '/edit');
      // Validate the item and add an eventual error message.
      $description .= _indicators_import_validate($datasets, $source);

      $options[$source->tid] = $description;
    }
  }
  // Do we have any options left?
  if (!empty($options)) {
    $prune['source_files'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Choose file to import'),
      '#options' => $options,
      '#required' => TRUE,
    );
    $options = array();
  }

  $help = '';
  if ($newer) {
    $help = '<p class="new">' . t('There are newer versions of a source file available, please import the corresponding dictionaries.') . '</p>';
  }

  $form['help'] = array(
    '#markup' => $help
    . '<p>' . t('Notice that an import can take a few minutes. So please import one dataset at a time!') . '</p>'
    . '<p>' . l(t('Edit !name taxonomy', array('!name' => INDICATORS_VOC_DICTIONARY)), 'admin/structure/taxonomy/' . INDICATORS_VOC_DICTIONARY) . '</p>'
    . '<p>' . t('Please ensure that the taxonomy has two nested levels at maximum.') . '</p>',
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Import'),
  );

  // Get the path to the module.
  $path = drupal_get_path('module', 'indicators_import');
  // Attach the CSS and JS to the form.
  $form['#attached'] = array(
    'css' => array(
      'type' => 'file',
      'data' => $path . '/indicators_import.css',
    ),
  );

  return $form;
}

/**
 * Run import on chosen files.
 */
function indicators_import_dic_form_submit($form, &$form_state) {
  $time_start = microtime(TRUE);

  // Read the list of source files.
  $source_files = _indicators_import_indicators_list(INDICATORS_VOC_DICTIONARY);

  // Get the form values.
  foreach ($form_state['values']['source_files'] as $tid) {
    // Find the corresponding Dictionary.
    foreach ($source_files as $source_file) {
      if ($source_file->tid == $tid) {
        list($rows, $records, $skipped) = _indicators_import_dic_import_save($source_file);
        $message = t('Processed @url. @records records inserted.',
          array(
            '@url' => $source_file->field_es_source_url[LANGUAGE_NONE][0]['value'],
            '@records' => $records,
            '@skipped' => $skipped,
          )
        );
        if ($rows > 0) {
          drupal_set_message($message);
        }
        else {
          drupal_set_message($message, 'warning');
        }
        break;
      }
    }
  }

  $time_end = microtime(TRUE);
  $execution_time = round($time_end - $time_start);  /* Time in seconds. */
  drupal_set_message(t('Import processed @lines lines in @seconds seconds.', array('@lines' => $rows, '@seconds' => $execution_time)));
}


/**
 * Read content from a gzipped CSV file and create data.
 *
 * @param object $source_file
 *   'Dictionary' term with additional fields.
 *
 * @return array
 *   Array(number of processed rows in the file, number of records created,
 *   number of records skipped) or FALSE if error.
 */
function _indicators_import_dic_import_save($source_file) {
  $url = htmlspecialchars_decode($source_file->field_es_source_url[LANGUAGE_NONE][0]['value']);
  setlocale(LC_ALL, "en_US.UTF-8");

  try {
    $raw_csv = file_get_contents($url);
  }
  catch (Exception $e) {
    drupal_set_message(t('Error opening %url : %message', array('%message' => $e->getMessage(), '%url' => $url)), 'error');
    return FALSE;
  }
  if ($raw_csv === FALSE) {
    // Error message already displayed.
    return FALSE;
  }
  // Split on line breaks in all operating systems.
  $lines = preg_split ('/$\R?^/m', $raw_csv);
  if ($lines[0][0] !== '"') {
    foreach ($lines as $i => &$line) {
      // Add double quotes around strings to avoid problems with UTF-8.
      $line = '"' . str_replace("\t", "\"\t\"", $line) . '"';
    }
  }

  // Load vocabulary.
  $voc_name = strtolower($source_file->field_es_taxonomy[LANGUAGE_NONE][0]['value']);
  $vocabulary = taxonomy_vocabulary_machine_name_load($voc_name);
  if (!$vocabulary) {
    // Create a new vocabulary.
    $vocabulary = _indicators_import_create_voc($voc_name, $source_file->name);
  }
  // Load vocabulary terms as tree.
  $voc_tree = taxonomy_get_tree($vocabulary->vid, 0, NULL, TRUE);

  // Process dictionary lines.
  $row = 0; $records = 0; $skipped = 0;
  foreach ($lines as $i => &$line) {
    // We don't use fgetcsv() because it has trouble dealing with UTF-8.
    $line = str_getcsv($line, "\t", '"');
    $records = _indicators_import_dic_save_line($vocabulary->vid, $voc_tree, $line, $row);
    ++$row;
  }

  // Update Dictionary's "updated" time.
  $updated = variable_get('indicators_import_updated', array());
  $updated[$source_file->tid] = time();
  variable_set('indicators_import_updated', $updated);

  return array($row, $records, $skipped);
}

/**
 * Save data to the Dictionary.
 *
 * @param int $vid
 *   Vocabulary id.
 * @param array $vocabulary
 *   Tree of terms.
 * @param array $line
 *   A line of records to process; it comes from CSV file.
 * @param int $l
 *   Line number.
 *
 * @return int
 *   Number of records written to the $vocabulary
 */
function _indicators_import_dic_save_line($vid, array &$vocabulary, array $line, $l, $charset = 'ANSI') {
  static $written;  /* Number of written records. */
  if (empty($written)) {
    $written = 0;
  }

  $code = $line[0];
  $name = trim($line[1]);

  // We don't allow terms with code but without name.
  if ($name == '') {
    return 0;
  }

  // The input file is coded in ANSI. Translate it to UTF8
  if ($charset == 'ANSI') {
    $name = utf8_encode($name);
  }
  // Check whether the term exists already.
  $insert = TRUE;
  foreach ($vocabulary as $term7) {
    if (isset($term7->field_es_code[LANGUAGE_NONE][0]['value'])) {
      if ($code == $term7->field_es_code[LANGUAGE_NONE][0]['value']) {
        $term = $term7;
        $insert = FALSE;  /* Yes, it exists. */
        break;
      }
    }
  }
  $save = FALSE;
  // Insert a new term ?
  if ($insert) {
    $term = new stdClass();
    $term->vid = $vid;
    $term->language = LANGUAGE_NONE;
    $term->format = 'plain_text';
    $term->name = truncate_utf8($name, 255, FALSE, TRUE);
    $term->description = $code;  /* Description = code. */
    $term->field_es_code[LANGUAGE_NONE][0]['value'] = $code;
    $term->field_es_code[LANGUAGE_NONE][0]['format'] = NULL;
    $term->field_es_code[LANGUAGE_NONE][0]['safe_value'] = $code;
    $save = TRUE;
  }
  // Update an existing term.
  else {
    // Any change to the term?
    if ($name != $term->name) {
      $term->name = truncate_utf8($name, 255, FALSE, TRUE);
      $save = TRUE;
    }
  }
  // Should the term be saved?
  if ($save) {
    // Return SAVED_NEW or SAVED_UPDATED.
    taxonomy_term_save($term);
    ++$written;
  }

  return $written;
}
