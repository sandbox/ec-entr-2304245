<?php

/**
 * @file
 * Default theme implementation to display an array of (label : value).
 *
 * Available variables:
 * - string $html_tag: HTML tag for both label and value (default is "td")
 * - string $label_class: CSS class of the Label
 * - string $value_class: CSS class of the Value
 * - array $label: Label
 * - array $value: Data
 * - array $unit: Unit
 *
 * @ingroup themeable
 */
 $label_class = trim('label ' . $label_class);
 $arrows = array(
   'up' => theme('image', array(
      'path' => drupal_get_path('module', 'indicators_query') . '/templates/indicator-up.png',
      'width' => 8,
      'height' => 9,
      'alt' => t('Up'),
      'title' => t('Up'),
    )),
   'down' => theme('image', array(
      'path' => drupal_get_path('module', 'indicators_query') . '/templates/indicator-down.png',
      'width' => 8,
      'height' => 9,
      'alt' => t('Down'),
      'title' => t('Down'),
    )),
 );
?>
<?php foreach ($value as $i => $item): ?>
  <<?php print $html_tag; ?> class="<?php print $label_class; ?>"><?php print $label[$i]; ?>:</<?php print $html_tag; ?>>
  <<?php print $html_tag; ?> class="value">
    <?php if (!empty($value_class[$i])): ?><span class="<?php print $value_class[$i]; ?>"><?php print $arrows[$value_class[$i]]; ?> <?php endif; ?>
<?php print $value[$i]; ?>
<?php if (!empty($value_class[$i])): ?></span><?php endif; ?>
    <?php if (!empty($unit[$i])): ?> <span class="unit"><?php print $unit[$i]; ?></span><?php endif; ?>
  </<?php print $html_tag; ?>>
<?php endforeach; ?>
