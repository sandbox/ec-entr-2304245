Indicators module
=================

DESCRIPTION
------------
Indicators module imports and graphically presents statistical data.
It is intended for use by European Commission Drupal community.

Features include:
 * Automatically import statistical data from remote CSV files
 * Support of input data in Eurostat [1] and Statplanet [2] formats
 * Display statistical data in charts
 * Country picker as map
 * Use Panels to build your page

REQUIREMENTS
------------
Drupal 7.x
Data 7.x
Taxonomy 7.x

Charts modules:
Flot
d3.js
FusionCharts (recommended) [3]

Charts/Maps libraries:
d3
flot
FusionCharts [3]
FusionMaps [4]
mapper.js
OpenLayers [5]


INSTALLATION
------------
1.  Place Indicators module into your modules directory.
    This is normally the "sites/all/modules/custom" directory.

2.  Go to admin/modules. Enable the modules.
    The Indicators modules is found in the ENTR section.

3.  Setup the module in admin/config/entr/indicators

4.  Define indicator terms corresponding to input files

5.  Import statistical data from in admin/config/entr/import

6.  Check imported data in admin/structure/data

7.  Define Q&Ds (Query and Displays) corresponding to the indicators you wish
    to display

8.  Install and enable the desired Charts module(s) and corresponding libraries

8.  Using Panels, create a page and add Q&D


UPGRADING
---------

When a statistical data file is updated on Eurostat file, it gets automatically
imported via a CRON job. A notification email is then sent. In case the
structure of the file is marked as changed, the import must be done manually:
admin/config/entr/import


FEATURES
--------
The detailed manual for developers is in development.
Visit the project page on Drupal.org:

https://www.drupal.org/sandbox/ec-entr/2304245

--------------------------------------------------------------------------------


CHANGE LOG
----------


AUTHORS
-------
EC-GROW - https://www.drupal.org/u/ec-grow
(GROW-DRUPAL-ORG@ec.europa.eu)

Rouslan Sorokine - https://www.drupal.org/u/rsorokine


REFERENCES
----------
[1] http://ec.europa.eu/eurostat
[2] http://www.statsilk.com/software/statplanet
[3] http://ec.europa.eu/ipg/services/interactive_services/charts
[4] http://ec.europa.eu/ipg/services/interactive_services/simple-maps
[5] http://openlayers.org
