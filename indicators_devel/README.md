Indicators Devel Module
=======================

This module is a starting place for Indicators Development support.

In order to debug indicators query and displays, it is helpful to see
all data generated. This module defines the display functions.

How to use it: Install it and enable it. Then in the Panel which uses
Indicators Query and Display blocks, enable "debug" mode.

AUTHORS
-------
EC-GROW - https://www.drupal.org/u/ec-grow
(GROW-DRUPAL-ORG@ec.europa.eu)

Rouslan Sorokine - https://www.drupal.org/u/rsorokine
