<?php

/**
 * @file
 * Admin forms for Indicators Query and Display.
 */

/**
 * Build an administration form for Indicators.
 */
function indicators_query_form($form_values = NULL) {
  $form = array();

  $form['indicators_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Indicators settings'),
    '#weight' => -1,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['indicators_settings']['indicators_table_int_size'] = array(
    '#type' => 'select',
    '#title' => t('Integer field size'),
    '#options' => array(
      'small' => t('small: 2 bytes: 0..65,535'),
      'medium' => t('medium: 3 bytes: 0..16,777,215'),
      'normal' => t('normal: 4 bytes: 0..4,294,967,295'),
      'big' => t('big: 8 bytes: 0..18,446,744,073,709,551,615'),
    ),
    '#description' => t('Size of the taxonomy reference field. Smaller size means smaller database. Be sure that your taxonomy database size does not reach the limit!'),
    '#default_value' => variable_get('indicators_table_int_size', 'normal'),
  );

  $form['indicators_settings']['indicators_mysql_engine'] = array(
    '#type' => 'select',
    '#title' => t('MySQL engine'),
    '#options' => array(
      'MyISAM' => t('MyISAM'),
      'InnoDB' => t('InnoDB'),
    ),
    '#description' => t('MyISAM will help you to save space. InnoDB is the default engine in Drupal.'),
    '#default_value' => variable_get('indicators_mysql_engine', 'MyISAM'),
  );

  $form['indicators_settings']['indicators_query_fusioncharts_path'] = array(
    '#type' => 'textfield',
    '#title' => t('FusionCharts path'),
    '#description' => t('Base url pointing to JS and Flash files. An HTTP site should point to HTTP resources and vice versa. Please add trailing slah.'),
    '#default_value' => variable_get('indicators_query_fusioncharts_path', 'https://europa.eu/webtools/libs/fusioncharts/js/fusioncharts.js'),
  );

  $form['indicators_settings']['indicators_query_googlemaps_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Google Maps API url'),
    '#description' => t('Base url pointing to JS file pf Google Maps. An HTTP site should point to HTTP resources and vice versa.'),
    '#default_value' => variable_get('indicators_query_googlemaps_path', 'http://maps.google.com/maps/api/js?v=3.6&sensor=false'),
  );

  $form['help'] = array(
    '#type' => 'item',
    '#title' => t('Useful links'),
    '#markup' => '<p>' .
    l(t('Edit Indicators taxonomy'), 'admin/structure/taxonomy/' . INDICATORS_VOC_INDICATOR) . '</p><p>' .
    l(t('Edit Data tables'), 'admin/structure/data') . '</p><p>' .
    l(t('Edit Dictionaries taxonomy'), 'admin/structure/taxonomy/' . INDICATORS_VOC_DICTIONARY) . '</p><p>' .
    l(t('Edit Query and Displays taxonomy'), 'admin/structure/taxonomy/' . INDICATORS_QUERY_VOC_QD) . '</p><p>' .
    l(t('Edit Panels'), 'admin/structure/pages') . '</p>',
  );

  return system_settings_form($form);
}
