<?php

/**
 * @file
 * Provide Geo (OpenLayers) interface for Indicators Query and Display Filter.
 *
 * @see http://openlayers.org/
 */

/**
 * CTools data.
 *
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'single' => TRUE,
  'title' => t('Indicators Geo Filter'),
  'icon' => 'icon_maps.png',
  'description' => t('Show a map to fill in geo fields of an exposed filter for a Statistical Data Query and Display.'),
  'category' => t('Indicators'),
  'edit form' => 'indicators_query_geo_edit_form',
  'render callback' => 'indicators_query_geo_render',
  'admin info' => 'indicators_query_geo_admin_info',
  'defaults' => array(
    'geo_id' => '',
    'help' => '',
    'display_style_width' => '',
    'display_style_height' => '',
    'geo_url' => '',
    'geo_use_google_osm' => '',
    'geo_use_google_satellite' => '',
    'geo_use_google_terrain' => '',
    'geo_use_google_roadmap' => '',
    'geo_use_google_hybrid' => '',
    'collapse' => TRUE,
    'max_nuts_level' => 3,
    'geo_lon' => 9,
    'geo_lat' => 56,
    'geo_zoom' => 4,
  ),
);

/**
 * CTools data.
 *
 * Admin info callback for panel pane.
 */
function indicators_query_geo_admin_info($subtype, $conf, $contexts) {
  if (!empty($conf)) {
    $block = new stdClass();
    $block->title = t('Query and Display Geo field helper for DOM element %name.', array(
      '%name' => $conf['geo_id'],
    ));
    $block->content = '';
    return $block;
  }
}

/**
 * CTools data.
 *
 * Edit form callback for the content type.
 */
function indicators_query_geo_edit_form($form, &$form_state) {
  $conf = &$form_state['conf'];

  $form['help'] = array(
    '#title' => t('Additional text to display'),
    '#description' => t('This text will be displayed below the title.'),
    '#type' => 'textfield',
    '#default_value' => $conf['help'],
    '#required' => FALSE,
  );

  $form['geo_url'] = array(
    '#title' => t('NUTS0 map file name'),
    '#description' => t('Vector map file name in .geojson format. The file must be located in sites/all/libraries/geomaps folder.'),
    '#type' => 'textfield',
    '#default_value' => $conf['geo_url'],
    '#required' => TRUE,
  );

  $form['max_nuts_level'] = array(
    '#title' => t('Maximum NUTS level'),
    '#description' => t('From 0 to 3. 0 = country level, 3 = the smallest region level'),
    '#type' => 'radios',
    '#default_value' => $conf['max_nuts_level'],
    '#options' => array(
      0 => t('NUTS 0'),
      1 => t('NUTS 1'),
      2 => t('NUTS 2'),
      3 => t('NUTS 3'),
    ),
  );

  $form['geo_lon'] = array(
    '#title' => t('Default Longitude'),
    '#description' => t('A number'),
    '#type' => 'textfield',
    '#size' => 10,
    '#maxlength' => 10,
    '#default_value' => $conf['geo_lon'],
    '#required' => TRUE,
  );

  $form['geo_lat'] = array(
    '#title' => t('Default Latitude'),
    '#description' => t('A number'),
    '#type' => 'textfield',
    '#size' => 10,
    '#maxlength' => 10,
    '#default_value' => $conf['geo_lat'],
    '#required' => TRUE,
  );

  $form['geo_zoom'] = array(
    '#title' => t('Default Zoom'),
    '#description' => t('A number'),
    '#type' => 'textfield',
    '#size' => 10,
    '#maxlength' => 10,
    '#default_value' => $conf['geo_zoom'],
    '#required' => TRUE,
  );

  $form['geo_id'] = array(
    '#title' => t('DOM id of the element holding the geo code value.'),
    '#description' => t('This element (usually an input) will be automatically updated when the user will pick up a geo region on the map.'),
    '#type' => 'textfield',
    '#default_value' => $conf['geo_id'],
    '#required' => TRUE,
  );

  $gmap_types = array(
    'google_osm',
    'google_satellite',
    'google_terrain',
    'google_roadmap',
    'google_hybrid',
  );
  foreach ($gmap_types as $gmap_type) {
    $form['geo_use_' . $gmap_type] = array(
      '#title' => t('Use Google %type', array('%type' => strtoupper(substr($gmap_type, 7)))),
      '#description' => t('If you choose Yes, the following JavaScript file will be included: !url',
        array(
          '!url' => l(
          variable_get('indicators_query_googlemaps_path', 'http://maps.google.com/maps/api/js?v=3.6&sensor=false'),
          variable_get('indicators_query_googlemaps_path', 'http://maps.google.com/maps/api/js?v=3.6&sensor=false')),
        )
      ),
      '#type' => 'checkbox',
      '#default_value' => $conf['geo_use_' . $gmap_type],
      '#required' => FALSE,
    );
  }

  $form['display_style_width'] = array(
    '#title' => t('Display style width'),
    '#description' => t('CSS width, for example 300px or 100%'),
    '#type' => 'textfield',
    '#default_value' => $conf['display_style_width'],
    '#required' => TRUE,
  );
  $form['display_style_height'] = array(
    '#title' => t('Display style height'),
    '#description' => t('CSS height, for example 200px'),
    '#type' => 'textfield',
    '#default_value' => $conf['display_style_height'],
    '#required' => TRUE,
  );
  $form['collapse'] = array(
    '#title' => t('Collapse fieldset'),
    '#description' => t('Collapse fieldset when geo code is set'),
    '#type' => 'checkbox',
    '#default_value' => $conf['collapse'],
  );

  return $form;
}

/**
 * The submit form stores the data in $conf.
 */
function indicators_query_geo_edit_form_submit($form, &$form_state) {
  foreach (array_keys($form_state['plugin']['defaults']) as $key) {
    if (isset($form_state['values'][$key])) {
      $form_state['conf'][$key] = $form_state['values'][$key];
    }
  }
}

/**
 * Run-time rendering of the body of the block (content type).
 *
 * See ctools_plugin_examples for more advanced info.
 */
function indicators_query_geo_render($subtype, $conf, $panel_args, $context = NULL) {
  // Get conf vars, pass them to JS:
  $settings = $conf;
  $settings['geo_url'] = url(libraries_get_path('geomaps') . '/' . $conf['geo_url'], array());

  $width = $conf['display_style_width'];
  $height = $conf['display_style_height'];

  // Create block.
  $block = new stdClass();

  // Initial content is blank.
  $block->title = '';

  // Add map div to block.
  $block->content  = indicators_geo_picker_html($width, $height, $conf['help'], $settings);
  return $block;
}

/**
 * HELPER function: Theme Geo picker (OpenLayers) block.
 *
 * @param string $width
 *   Block width (e.g: 100, 100px, 100%).
 * @param string $height
 *   Block height (e.g: 100, 100px, 100%).
 * @param string $help
 *   Help.
 * @param array $settings
 *   Variables passed on to JS:
 *   'geo_id': DOM id of the option list to update on a click.
 *     If empty, the user will be redirected to 'path'
 *   'map': Map file path
 *   'data': Map configuration data
 *   'path': url (absolute or internal) to go when user clicks on a region.
 *     Region code will be appended to it.
 *
 * @return string
 *   html code (rendered form).
 */
function indicators_geo_picker_html($width, $height, $help, array $settings) {
  $inner_html = '<div id="map" style="width: ' . $width . '; height: ' . $height . '">';
  // Add JS files to the page.
  drupal_add_js(libraries_get_path('openlayers') . '/OpenLayers.js');
  if (!empty($settings['geo_use_google_osm'])
  || !empty($settings['geo_use_google_satellite'])
  || !empty($settings['geo_use_google_terrain'])
  || !empty($settings['geo_use_google_roadmap'])
  || !empty($settings['geo_use_google_hybrid'])) {
    drupal_add_js(variable_get('indicators_query_googlemaps_path', 'http://maps.google.com/maps/api/js?v=3.6&sensor=false'), 'external');
  }
  drupal_add_js(drupal_get_path('module', 'indicators_query') . '/plugins/content_types/indicators_geo.js');
  drupal_add_css(drupal_get_path('module', 'indicators_query') . '/plugins/content_types/indicators_geo.css');

  // We also need to load all Countries/Regions to pass the names
  // to the OpenLayers map JS for showing the names on hover.
  $ol_voc = taxonomy_vocabulary_machine_name_load($settings['geo_vocabulary']);
  $arr_terms = taxonomy_term_load_multiple(array(), array('vid' => $ol_voc->vid));
  $js_geo = 'var arr_regions_info = new Array();' . "\n";
  foreach ($arr_terms as $term) {
    // Geo code -> geo name.
    $js_geo .= 'arr_regions_info["' . $term->description . '"] = "' . $term->name . '";' . "\n";
  }
  drupal_add_js($js_geo, array('type' => 'inline', 'scope' => 'header'));

  // Add JS settings.
  drupal_add_js(array('indicators_query' => $settings), 'setting');

  // Generate form.
  $html = drupal_render(drupal_get_form('indicators_geo_filter_html_form', $inner_html, $help));
  $html .= '</div>';
  $html .= '<div id="ol_loading">' . t('Loading...') . '</div>';
  $html .= '<div id="curr_ctrl">';
  $html .= '  <div id="curr_country_ctrl">- clear current country <span id="curr_country"></span></div>';
  $html .= '  <div id="curr_nuts_ctrl">- back to NUTS1 (current NUTS<span id="curr_nuts"></span>)</div>';
  $html .= '</div>';
  $html .= '<div id="region_tooltip"></div>' . "\n";
  return $html;
}
