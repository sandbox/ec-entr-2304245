<?php
/**
 * @file
 * This template is used to print a single field in a view.
 *
 * It is not actually used in default Views, as this is registered as a theme
 * function which has better performance. For single overrides, the template is
 * perfectly okay.
 *
 * Variables available:
 * - $view: The view object
 * - $field: The field handler object that can process the input
 * - $row: The raw SQL result that can be used
 * - $output: The processed output that will normally be used.
 *
 * When fetching output from the $row, this construct should be used:
 * $data = $row->{$field->field_alias}
 *
 * The above will guarantee that you'll always get the correct data,
 * regardless of any changes in the aliasing that might happen if
 * the view is modified.
 */
// The field is numeric and probably a taxonomy term.
if (!empty($row->{$field->field_alias})
    && !in_array($field->field, array('eid'))
    && $field->definition['handler'] == 'views_handler_field_numeric'
) :
    // Get list of Indicator data tables.
    // They match the corresponding admin views names.
    $data_views = indicators_import_datasets_list();
    // The view is Indicator's admin.
    if (isset($data_views[$view->name])) :
        // Is the field 'int, unsigned'?
        if ($data_views[$view->name]->table_schema['fields'][$field->field]['type'] == 'int'
            && $data_views[$view->name]->table_schema['fields'][$field->field]['unsigned'] == 1
        ) :
            // It is a taxonomy term tid then.
            $term = taxonomy_term_load($row->{$field->field_alias});
            if ($term) :
                $code = $term->field_es_code[LANGUAGE_NONE][0]['value'];
                $code = truncate_utf8($code, 20, FALSE, TRUE);
                $output = '<span title = "' . $term->name . '">' . $code . '</span>';
            endif;
        endif;
    endif;
endif;
?>
<?php print $output; ?>
