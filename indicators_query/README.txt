INTRODUCTION
------------

Indicators Query and Display module present graphically statistical data.
It is intended for use by European Commission Drupal community.

Features include:
 * Display statistical data in charts (FusionCharts)
 * Country picker as map
 * Use Panels to build your page

REQUIREMENTS
------------

This module requires the following modules:
 
 * FusionCharts (https://www.drupal.org/project/fusioncharts)
 * Features (https://www.drupal.org/project/features)
 * i18n (https://www.drupal.org/project/i18n) 
 * Indicators (https://www.drupal.org/sandbox/ec-entr/2304245)

This module requires the following libraries:

 * FusionCharts (http://www.fusioncharts.com)

INSTALLATION
------------
 
 * Place Indicators Query and Display module into your modules directory.
   This is normally the "sites/all/modules/custom" directory.

 * Go to admin/modules. Enable the modules.
   The Indicators Query and Display module is found in the ENTR section.

CONFIGURATION
-------------

 * Define Query and Displays corresponding to the indicators you wish
   to display (admin/structure/taxonomy/es_query)

 * Install and enable the desired Charts module(s) and corresponding libraries

   - FusionCharts: Copy this code to your default theme's template.php:

     function yourtheme_theme_fusionchart($variables) {
       if (module_exists('indicators_query')) {
         return indicators_query_theme_fusionchart($variables);
       }
     }

     Otherwise your FusionCharts will display "TRIAL VERSION" message.

   The detailed manual for developers is in development.
   Visit the project page on Drupal.org:
   https://www.drupal.org/sandbox/ec-entr/2304245

 * Using Panels, create a page and add Q&D

MAINTAINERS
-----------

  * EC-GROW - https://www.drupal.org/u/ec-grow
    (GROW-DRUPAL-ORG@ec.europa.eu)

  * Rouslan Sorokine - https://www.drupal.org/u/rsorokine
  * Thibaut Van Bellinghen - https://www.drupal.org/u/tvb1507