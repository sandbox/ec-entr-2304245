<?php

/**
 * @file
 * Admin forms for Indicators.
 */

/**
 * Provide a single block from the administration menu as a page.
 *
 * @see system_admin_menu_block_page()
 */
function indicators_admin_menu_block_page() {
  $item = menu_get_item();
  $content = system_admin_menu_block($item);
  if ($content) {
    $output = theme('admin_block_content', array('content' => $content));
  }
  else {
    $output = t('You do not have any administrative items.');
  }
  return $output;
}

/**
 * Build an administration form for Indicators.
 */
function indicators_form($form_values = NULL) {
  $form = array();

  $form['indicators_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Indicators settings'),
    '#weight' => -1,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['indicators_settings']['indicators_table_int_size'] = array(
    '#type' => 'select',
    '#title' => t('Integer field size'),
    '#options' => array(
      'small' => t('small: 2 bytes: 0..65,535'),
      'medium' => t('medium: 3 bytes: 0..16,777,215'),
      'normal' => t('normal: 4 bytes: 0..4,294,967,295'),
      'big' => t('big: 8 bytes: 0..18,446,744,073,709,551,615'),
    ),
    '#description' => t('Size of the taxonomy reference field. Smaller size means smaller database. Be sure that your taxonomy database size does not reach the limit!'),
    '#default_value' => variable_get('indicators_table_int_size', 'normal'),
  );

  $form['indicators_settings']['indicators_mysql_engine'] = array(
    '#type' => 'select',
    '#title' => t('MySQL engine'),
    '#options' => array(
      'MyISAM' => t('MyISAM'),
      'InnoDB' => t('InnoDB'),
    ),
    '#description' => t('MyISAM will help you to save space. InnoDB is the default engine in Drupal.'),
    '#default_value' => variable_get('indicators_mysql_engine', 'MyISAM'),
  );

  $help = '<p>' . l(t('Edit Indicators taxonomy'), 'admin/structure/taxonomy/' . INDICATORS_VOC_INDICATOR) . '</p>';
  $help .= '<p>' . l(t('Edit Data tables'), 'admin/structure/data') . '</p>';
  $help .= '<p>' . l(t('Edit Dictionaries taxonomy'), 'admin/structure/taxonomy/' . INDICATORS_VOC_DICTIONARY) . '</p>';
  $help .= '<p>' . l(t('Edit Panels'), 'admin/structure/pages') . '</p>';
  $form['help'] = array(
    '#type' => 'item',
    '#title' => t('Useful links'),
    '#markup' => $help,
  );

  return system_settings_form($form);
}
