<?php

/**
 * @file
 * Check for updated indicators (datasets).
 */

/**
 * Check for updates and notify webmaster.
 *
 * @see indicators_import_check_updates()
 */
function _indicators_import_check_updates() {
  $available_updates = _indicators_import_check_run();
  if (!empty($available_updates)) {
    $title = t('There are newer versions of statistical data, the corresponding datasets are being imported:');
    $message = theme('item_list', array(
      'items' => $available_updates,
      'title' => $title,
      'type' => 'ul'
    ));
    $message .= "\n\n" . t('Please check the result of the import by visiting') . ' ' . l(t('ENTR Data Import'), 'admin/config/entr/import');
    _indicators_import_send_mail($message);
  }
}

/**
 * Send email to the site's webmaster.
 */
function _indicators_import_send_mail($message) {
  $to = variable_get('indicators_import_to', variable_get('site_mail', ''));
  $from = variable_get('indicators_import_from', variable_get('site_mail', ''));
  $subject = '[' . variable_get('site_name', '') . '] ' . t('Statistical data updates available');
  $lang = language_default();
  $params = array('message' => $message, 'subject' => $subject);
  $message = drupal_mail('indicators_import', 'dummy', $to, $lang, $params, $from);
}

/**
 * List all datasets which have an update on Eurostat site.
 */
function _indicators_import_check_run() {
  module_load_include('inc', 'indicators_import', 'indicators_import.data.admin');
  // This list source files corresponds to the taxonomy tree 'Indicator',
  // where only tree leaves correspond to remote files and tree branches are
  // only used to neatly organize those in fieldsets.
  $source_files = _indicators_import_indicators_list();
  $datasets = data_get_all_tables();
  $options = array();

  // Get info about when indicators have been last imported.
  $updated = variable_get('indicators_import_updated', array());

  foreach ($source_files as $source) {
    if (!empty($source->field_es_code)) {
      $description = $source->field_es_code[LANGUAGE_NONE][0]['value'] . ' - ' . $source->description . '. ';
      if (!empty($updated[$source->tid])) {
        $description .= t('Imported on @date.', array('@date' => date('d/m/Y', $updated[$source->tid]))) . ' ';
      }
      if (!empty($source->field_es_remote_newer[LANGUAGE_NONE][0])
        && !empty($source->field_es_remote_info[LANGUAGE_NONE][0]['date'])) {
        $description .= t('A newer version is available') . ' : ';
        $description .= '(' . $source->field_es_remote_info[LANGUAGE_NONE][0]['date'] . ')';
        // Validate the item and add an eventual error message.
        $description .= _indicators_import_validate($datasets, $source);
        $options[$source->tid] = $description;
      }
    }
  }

  return $options;
}

/**
 * OFF-LINE BATCH IMPORT PROCESS.
 *
 * Create a function which mimics form submission, to be triggered by CRON.
 */

/**
 * This function call is triggered by CRON.
 */
function indicators_import_offline_cron_run() {
  $form_state = array();
  $form_state['values']['run_offline'] = 1;
  $form_state['values']['op'] = 'Test';
  $form_state['values']['submit'] = 'Test';
  drupal_form_submit('indicators_import_offline_form', $form_state);
}
/**
 * Form to run import on chosen files, off-line batch.
 */
function indicators_import_offline_form($form, &$form_state) {
  $form['run_offline'] = array(
    '#type' => 'hidden',
    '#value' => 1,
  );
  return $form;
}

/**
 * Run import on chosen files, off-line batch.
 */
function indicators_import_offline_form_submit($form, &$form_state) {
  module_load_include('inc', 'indicators_import', 'indicators_import.data.admin');
  // Read the list of source files.
  $source_files = _indicators_import_indicators_list();

  // Check form validity.
  if (!empty($form_state['values']['run_offline'])) {
    // For all indicators we have an update for.
    foreach ($source_files as $source_file) {
      if (!empty($source_file->field_es_remote_newer[LANGUAGE_NONE][0])) {
        $structure_date_stamp = $imported_date_stamp = 0;
        // Check first if the file structure has changed since last import.
        if (!empty($source_file->field_es_remote_info[LANGUAGE_NONE][0]['size'])) {
          if (strpos($source_file->field_es_remote_info[LANGUAGE_NONE][0]['size'], 'Structure changed on ') === 0) {
            $structure_date = substr($source_file->field_es_remote_info[LANGUAGE_NONE][0]['size'], strlen('Structure changed on '));
            $structure_date_stamp = _indicators_import_date_timestamp($structure_date);
            $imported_date = $source_file->field_es_updated[LANGUAGE_NONE][0]['date'];
            $imported_date_stamp = _indicators_import_date_timestamp($imported_date);
          }
        }
        // We can not import automatically if the file structure has changed.
        if ($structure_date_stamp >= $imported_date_stamp) {
          drupal_set_message(t('Aborted: Batch to import %name as the file structure has changed on %date.', array('%name' => $source_file->name, '%date' => $structure_date)));
          watchdog(
            'indicators import',
            'Aborted automatic import of %name because the file structure has changed on %date. Please check the file and import manually.',
            array(
              '%name' => $source_file->name,
              '%date' => $structure_date,
            )
          );
        }
        else {
          // Start a batch to import the dataset.
          _indicators_import_data_import_save($source_file);
          drupal_set_message(t('Batch to import %name has started.', array('%name' => $source_file->name)));
        }
      }
    }
  }
}

/**
 * Helper function to convert date from '16/05/2014' format to timestamp.
 */
function _indicators_import_date_timestamp($date, $format = '%d/%m/%Y') {
  $timestamp = FALSE;
  $ts = strptime($date, $format);
  if ($ts) {
    $timestamp = mktime($ts['tm_hour'], $ts['tm_min'], $ts['tm_sec'], $ts['tm_mon'] + 1, $ts['tm_mday'], ($ts['tm_year'] + 1900));
  }
  return $timestamp;
}
