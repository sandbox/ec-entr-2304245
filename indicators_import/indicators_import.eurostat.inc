<?php

/**
 * @file
 * Import function for Eurostat Indicators Import (datasets).
 */

/**
 * Save data to SQL table using "data" module, for Eurostat format.
 *
 * @param object $table
 *   SQL table to add records to.
 * @param array $lines
 *   Records to process; it comes from CSV file.
 * @param int $l
 *   Starting line number.
 * @param array $settings
 *   Array(Indicator tid, fields, filters, vocabularies).
 * @param int $skipped
 *   Number of skipped (filtered out) records.
 *
 * @return array
 *   Array(number of records written to the table, number of records skipped).
 */
function _indicators_import_data_save_line_eurostat(&$table, array &$lines, &$l, array &$settings, &$skipped) {
  static $times;
  static $geo_codes;
  static $var_names;
  static $written;  /* Number of written records. */
  // Keep missing vocabulary codes, to suppress duplicate error messages.
  static $missing;
  static $data_fields;
  static $first_line;

  // Get list of fields of the data table.
  if (empty($data_fields)) {
    $data_fields = array_keys(data_get_field_definitions());
  }

  // Process batch lines.
  foreach ($lines as $batch => $line) {
    // Calculate data records out of imported line.
    $data = array();
    // First line has either (1) times or (2) geo codes.
    if ($batch == 0 && $l == 0) {
      $written = 0;
      // See what first line holds as data.
      if (strpos($line[0], 'geo\time') !== FALSE) {
        $first_line = 'time';
        // $first_col = 'geo';
      }
      elseif (strpos($line[0], 'time\geo') !== FALSE) {
        $first_line = 'geo';
        // $first_col = 'time';
      }

      // Keep times (usually years) for later use.
      if ($first_line == 'time') {
        $times = $line;
        // Get rid of "indic_to,unit,activity,geo\time".
        unset($times[0]);
        foreach ($times as &$ttime) {
          $ttime = trim($ttime);
        }
      }
      // Keep geo codes for later use.
      elseif ($first_line == 'geo') {
        $geo_codes = $line;
        // Get rid of "indic_to,unit,activity,time\geo".
        unset($geo_codes[0]);
        foreach ($geo_codes as &$ggeo_code) {
          $ggeo_code = trim($ggeo_code);
        }
      }

      // Process "indic_to,unit,activity,geo\time".
      $var_names = explode(',', $line[0]);
      foreach ($var_names as &$nname) {
        // Get rid of "\time" in "geo\time" or of "\geo" in "time\geo".
        $nname = explode('\\', $nname);
        $nname = $nname[0];
      }
      $var_names = array_flip($var_names);
      // Time and Variable names are saved in static vars for later use.
      continue;
    }
    // Next lines have data.
    else {
      foreach ($line as $r => $row) {
        // First row has info about the line: B004,NBR,A001,AT
        if ($r == 0) {
          $vars = explode(',', $row);
        }
        else {
          // The cell may have been flagged.
          $row = explode(' ', $row);
          $number = $row[0];
          // @todo: there are maybe many flags attached to one cell; we keep the first one because of the SQL table structure
          $flag = isset($row[1]) ? substr($row[1], 0, 1) : NULL;
          // Parse number for special meaning.
          if (trim($number) == ':') {
            $number = NULL;
          }
          if ($number != NULL || $flag != NULL) {
            // Check if the record is filtered out.
            if (_indicators_import_filter_out($vars, $var_names, $settings['filters'])) {
              ++$skipped;
              // Skip the record.
              continue;
            }
            // Time field is in the first line.
            if ($first_line == 'time') {
              $time = $times[$r];
            }
            // Time field is in a column.
            elseif (isset($var_names['time'])) {
              $time = $vars[$var_names['time']];
            }
            // Time field is not used.
            else {
              $time = NULL;
            }
            // Geo field is in the first line.
            if ($first_line == 'geo') {
              $geo_code = $geo_codes[$r];
            }
            // Geo field is in a column.
            elseif (isset($var_names['geo'])) {
              $geo_code = $vars[$var_names['geo']];
            }
            // Geo field is not used.
            else {
              $geo_code = NULL;
            }
            // Validate Geo code.
            if ($geo_code) {
              // If geo code doesn't exist.
              if (!isset($settings['vocabularies'][INDICATORS_VOC_GEO][$geo_code])) {
                if (!isset($missing[INDICATORS_VOC_GEO][$geo_code])) {
                  drupal_set_message(t('Taxonomy term for the code %name of vocabulary @voc not defined.',
                    array('%name' => $geo_code, '@voc' => INDICATORS_VOC_GEO)), 'warning');
                  $missing[INDICATORS_VOC_GEO][$geo_code] = TRUE;
                }
                ++$skipped;
                // Skip the record.
                continue;
              }
            }
            // Create a record.
            $record = array(
              'value' => $number,
              'time' => $time,
              'flag' => $flag == NULL ? NULL : $settings['vocabularies'][INDICATORS_VOC_FLAG][$flag], /* tid of "Flag" term */
              'geo' => ($geo_code == NULL) ? NULL : $settings['vocabularies'][INDICATORS_VOC_GEO][$geo_code], /* tid of "Geo" term. */
              'source' => $settings['source'], /* tid of "Indicator" term. */
            );
            // Add parsed fields to the record.
            foreach ($var_names as $field_name => $i) {
              // If the field is not in the fields' list, skip it.
              if (!in_array($field_name, array_keys($settings['fields']))) {
                continue;
              }
              $field_type = $settings['fields'][$field_name];
              // Copy simple data field (varchar, float, etc.)
              if (in_array($field_type, $data_fields)) {
                $record[$field_name] = $vars[$var_names[$field_name]];
              }
              // Calculate taxonomy term's tid.
              elseif (substr($field_type, 0, 8) == 'taxonomy') {
                $voc = substr($field_type, 9);
                if (isset($settings['vocabularies'][$voc][$vars[$var_names[$field_name]]])) {
                  $record[$field_name] = $settings['vocabularies'][$voc][$vars[$var_names[$field_name]]];
                }
                else {
                  drupal_set_message(t('Taxonomy term %name of vocabulary @voc not defined.',
                    array('%name' => $vars[$var_names[$field_name]], '@voc' => $voc)), 'warning');
                }
              }
              else {
                drupal_set_message(t('Unsupported field (yet): @name. Import aborted.', array('@name' => $field_name)), 'error');
                return 0;
              }
            }
            // Add the record to the result.
            $data[] = $record;
          }
        }
      }
    }

    if (!empty($data)) {
      // Create an "INSERT TO" query for multiple records,
      // which will run much faster!
      $sql = 'INSERT INTO {' . $table->name . '} (' . implode(', ', array_keys($data[0])) . ') VALUES ';
      $i_max = count($data) - 1;
      foreach ($data as $i => $record) {
        $rec = '(';
        foreach ($record as $value) {
          $rec .= ($value == '' ? "NULL, " : "'$value', ");
        }
        if ($i == $i_max) {
          $sql .= substr($rec, 0, -2) . ');';
        }
        else {
          $sql .= substr($rec, 0, -2) . '), ';
        }
      }
      db_query($sql);
      $written += $i_max + 1;
    }
    ++$l;
  }
  // Clear up memory.
  unset($sql, $data, $rec, $voc, $time, $geo_code, $record, $r, $row, $flag, $number, $i, $i_max, $value, $field_name);

  // Number of written records.
  return $written;
}
