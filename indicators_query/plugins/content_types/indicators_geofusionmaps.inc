<?php

/**
 * @file
 * Provide Geo (FusionMaps) interface for Indicators Query and Display Filter.
 *
 * See Documentation in https://webtools.ec.europa.eu/fusionmapsxt/
 * See Code and Maps source in https://webtools.ec.europa.eu/fusionmapsxt/Maps/
 */

/**
 * CTools data.
 *
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'single' => TRUE,
  'title' => t('Indicators FusionMaps Filter'),
  'icon' => 'icon_maps.png',
  'description' => t('Show a map to fill in geo fields of an exposed filter for a Statistical Data Query and Display.'),
  'category' => t('Indicators'),
  'edit form' => 'indicators_query_geofm_edit_form',
  'render callback' => 'indicators_query_geofm_render',
  'admin info' => 'indicators_query_geofm_admin_info',
  'defaults' => array(
    'geo_id' => '',
    'help' => '',
    'display_style_width' => '',
    'display_style_height' => '',
    'map' => '',
    'data' => '',
  ),
);

/**
 * CTools data.
 *
 * Admin info callback for panel pane.
 */
function indicators_query_geofm_admin_info($subtype, $conf, $contexts) {
  if (!empty($conf)) {
    $block = new stdClass();
    $block->title = t('Query and Display Geo field helper for DOM element %name.', array(
      '%name' => $conf['geo_id'],
    ));
    $block->content = '';
    return $block;
  }
}

/**
 * CTools data.
 *
 * Edit form callback for the content type.
 */
function indicators_query_geofm_edit_form($form, &$form_state) {
  $conf = &$form_state['conf'];

  $form['help'] = array(
    '#title' => t('Additional text to display'),
    '#description' => t('This text will be displayed below the title.'),
    '#type' => 'textfield',
    '#default_value' => $conf['help'],
    '#required' => FALSE,
  );

  $form['map'] = array(
    '#title' => t('FusionMaps map file name'),
    '#description' => t('It is a .SWF file. The file must be located in sites/all/libraries/FusionMaps folder.'),
    '#type' => 'textfield',
    '#default_value' => $conf['map'],
    '#required' => TRUE,
  );

  $form['data'] = array(
    '#title' => t('Map configuration file'),
    '#description' => t('It is an .XML file describing the content of the map.'),
    '#type' => 'textfield',
    '#default_value' => $conf['data'],
    '#required' => TRUE,
  );

  $form['geo_id'] = array(
    '#title' => t('DOM id of the element holding the geo code value.'),
    '#description' => t('This element (usually an input) will be automatically updated when the user will pick up a geo region on the map.'),
    '#type' => 'textfield',
    '#default_value' => $conf['geo_id'],
    '#required' => TRUE,
  );

  $form['display_style_width'] = array(
    '#title' => t('Display style width'),
    '#description' => t('CSS width, for example 300px or 100%'),
    '#type' => 'textfield',
    '#default_value' => $conf['display_style_width'],
    '#required' => TRUE,
  );
  $form['display_style_height'] = array(
    '#title' => t('Display style height'),
    '#description' => t('CSS height, for example 200px'),
    '#type' => 'textfield',
    '#default_value' => $conf['display_style_height'],
    '#required' => TRUE,
  );

  return $form;
}

/**
 * The submit form stores the data in $conf.
 */
function indicators_query_geofm_edit_form_submit($form, &$form_state) {
  foreach (array_keys($form_state['plugin']['defaults']) as $key) {
    if (isset($form_state['values'][$key])) {
      $form_state['conf'][$key] = $form_state['values'][$key];
    }
  }
}

/**
 * Render FusionMaps block.
 *
 * Run-time rendering of the body of the block (content type)
 * See ctools_plugin_examples for more advanced info.
 */
function indicators_query_geofm_render($subtype, $conf, $panel_args, $context = NULL) {
  // Get conf vars, pass them to JS.
  $settings = $conf;
  $settings['map'] = libraries_get_path('FusionMaps') . '/' . $settings['map'];

  $width = $conf['display_style_width'];
  $height = $conf['display_style_height'];

  // Create block.
  $block = new stdClass();

  // Initial content is blank.
  $block->title = '';

  // Add map div to block.
  $block->content  = indicators_geofusionmaps_picker_html($width, $height, $conf['help'], $settings);
  return $block;
}

/**
 * HELPER function: Theme Geo picker (FusionMaps) block.
 *
 * @param string $width
 *   Block width (e.g: 100, 100px, 100%).
 * @param string $height
 *   Block height (e.g: 100, 100px, 100%).
 * @param string $help
 *   Help string.
 * @param array $settings
 *   Variables passed on to JS:
 *   'geo_id': DOM id of the option list to update on a click.
 *     If empty, the user will be redirected to 'path'
 *   'map': Map file path
 *   'data': Map configuration data
 *   'path': url (absolute or internal) to go when user clicks on a region.
 *     Region code will be appended to it.
 *
 * @return string
 *   Rendered form (html)
 */
function indicators_geofusionmaps_picker_html($width, $height, $help, array $settings) {
  $inner_html = '<div id="mapdiv" style="width: ' . $width . '; height: ' . $height . '">FusionMap</div>' . "\n";
  // Add FusionMaps Library JS.
  drupal_add_js(libraries_get_path('FusionMaps') . '/FusionCharts.js');
  drupal_add_js(drupal_get_path('module', 'indicators_query') . '/plugins/content_types/indicators_geofusionmaps.js');
  drupal_add_css(drupal_get_path('module', 'indicators_query') . '/plugins/content_types/indicators_geofusionmaps.css');
  // Add JS settings.
  drupal_add_js(array('indicators_query' => $settings), 'setting');
  // Generate form.
  $form = drupal_get_form('indicators_geo_filter_html_form', $inner_html, $help);
  return drupal_render($form);
}
