/**
 * @file
 * Indicators Query script.
 *
 * Autosubmit on change of X-axis options control (number of items displayed).
 */

(function ($) {
  Drupal.behaviors.indicatorsQuery = {
    attach: function (context) {
      $("select[name='x_axis_options']").change(function() {
        $(this).parent().parent().parent().submit();
      });
      $("select[name='x_axis_monthly']").change(function() {
        $(this).parent().parent().parent().submit();
      });
    }
  };
})(jQuery);
