<?php

/**
 * @file
 * Admin forms for Indicators Import (datasets).
 */

/* Number of input lines to process in one batch call. */
define('INDICATORS_IMPORT_DEFAULT_BATCH_SIZE', 100);

/**
 * Build a form for running a dataset import.
 */
function indicators_import_run_form($form_values = NULL) {
  // This list source files corresponds to the taxonomy tree 'Indicator',
  // where only tree leaves correspond to remote files and tree branches are
  // only used to neatly organize those in fieldsets.
  $source_files = _indicators_import_indicators_list();
  $datasets = data_get_all_tables();
  $form = array();
  $options = array();
  $prune = &$form;
  $newer = FALSE;

  // Get info about when indicators have been last imported.
  $updated = variable_get('indicators_import_updated', array());

  // Create form elements: nested fieldsets (2 levels maximum),
  // and options for file downloads.
  foreach ($source_files as $source) {
    // A branch? Create a nested fieldset.
    if (empty($source->field_es_code)) {
      // Do we have any previous options?
      if (!empty($options)) {
        $prune['source_files_' . $source->tid] = array(
          '#type' => 'checkboxes',
          '#title' => t('Choose file to import'),
          '#options' => $options,
          '#required' => FALSE,
        );
        $options = array();
      }
      // Level 0 branch?
      if ($source->parents[0] == 0) {
        $form['category_' . $source->tid] = array(
          '#type' => 'fieldset',
          '#title' => check_plain($source->name),
          '#collapsible' => TRUE,
          '#collapsed' => FALSE,
        );
        $prune = &$form['category_' . $source->tid];
      }
      // Level 1 branch.
      else {
        $form['category_' . $source->parents[0]]['category_' . $source->tid] = array(
          '#type' => 'fieldset',
          '#title' => check_plain($source->name),
          '#collapsible' => TRUE,
          '#collapsed' => FALSE,
        );
        $prune = &$form['category_' . $source->parents[0]]['category_' . $source->tid];
      }
    }
    // A leaf? Add to the file options of the last fieldset.
    else {
      // Compile an option line.
      $description = '';
      // Add source format logo.
      if (!empty($source->field_es_source_format[LANGUAGE_NONE][0]['value'])) {
        $logo = array(
          'height' => 32,
          'path' => drupal_get_path('module', 'indicators_import') . '/images/' . $source->field_es_source_format[LANGUAGE_NONE][0]['value'] . '-logo.png',
        );
        $description .= theme('image', $logo);
      }
      $description .= $source->field_es_code[LANGUAGE_NONE][0]['value'];
      $description .= ' - ' . $source->description;
      if (!empty($updated[$source->tid])) {
        $description .= '. ' . t('Imported on @date', array('@date' => date('d/m/Y', $updated[$source->tid])));
      }
      if (!empty($source->field_es_remote_newer[LANGUAGE_NONE][0])
        && !empty($source->field_es_remote_info[LANGUAGE_NONE][0]['date'])) {
        $description .= ' <span class="new">' . t('There is a newer version of this file available') . '</span> : ';
        $description .= '(' . $source->field_es_remote_info[LANGUAGE_NONE][0]['date'];
        $description .= ', ' . $source->field_es_remote_info[LANGUAGE_NONE][0]['size'] . ')';
        $newer = TRUE;
      }
      $description .= ' - ' . l(t('Taxonomy entry'), 'taxonomy/term/' . $source->tid . '/edit');
      if (!empty($source->field_es_data_table[LANGUAGE_NONE][0]['value'])) {
        $description .= ' - ' . l(t('Dataset fields'), 'admin/structure/data/edit/' . $source->field_es_data_table[LANGUAGE_NONE][0]['value']);
        $description .= ' - ' . l(t('Dataset values'), 'admin/content/data/view/' . $source->field_es_data_table[LANGUAGE_NONE][0]['value']);
      }
      // Validate the item and add an eventual error message.
      $description .= _indicators_import_validate($datasets, $source);

      $options[$source->tid] = $description;
    }
  }
  // Do we have any options left?
  if (!empty($options)) {
    $prune['source_files_0'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Choose file to import'),
      '#options' => $options,
      '#required' => FALSE,
    );
    $options = array();
  }

  $help = '';
  if ($newer) {
    $help = '<p class="new">' . t('There are newer versions of a source file available, please import the corresponding datasets.') . '</p>';
  }

  $form['help'] = array(
    '#markup' => $help
    . '<p>' . t('Notice that an import can take a few minutes. So please import one dataset at a time!') . '</p>'
    . '<p>' . l(t('Edit !name taxonomy', array('!name' => INDICATORS_VOC_INDICATOR)), 'admin/structure/taxonomy/' . INDICATORS_VOC_INDICATOR) . '</p>'
    . '<p>' . t('Please ensure that the taxonomy has two nested levels at maximum.') . '</p>',
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Import'),
  );

  // Get the path to the module.
  $path = drupal_get_path('module', 'indicators_import');
  // Attach the CSS and JS to the form.
  $form['#attached'] = array(
    'css' => array(
      'type' => 'file',
      'data' => $path . '/indicators_import.css',
    ),
  );
  return $form;
}

/**
 * Run import on chosen files.
 */
function indicators_import_run_form_submit($form, &$form_state) {
  // Read the list of source files.
  $source_files = _indicators_import_indicators_list();

  // Get the form values.
  foreach ($form_state['values'] as $field => $tids) {
    if (substr($field, 0, 13) == 'source_files_') {
      foreach ($tids as $tid) {
        if (!empty($tid)) {
          // Find the corresponding Indicator.
          foreach ($source_files as $source_file) {
            if ($source_file->tid == $tid) {
              // Import dataset.
              _indicators_import_data_import_save($source_file);
              break;
            }
          }
        }
      }
    }
  }
}


/**
 * Read content from a gzipped CSV file and create data.
 *
 * @param object $source_file
 *   'Indicator' term with additional fields.
 */
function _indicators_import_data_import_save($source_file) {

  // Gzipped file?
  $url = $source_file->field_es_source_url[LANGUAGE_NONE][0]['value'];
  $filename = $source_file->field_es_source_filename[LANGUAGE_NONE][0]['value'];

  // Init Batch processing.
  $batch = array(
    'title' => t('Importing @url', array('@url' => $url)),
    'finished' => '_indicators_import_data_finished', /* Callback. */
    'file' => drupal_get_path('module', 'indicators_import') . '/indicators_import.data.admin.inc',
  );

  // Load data table.
  $table_name = strtolower($source_file->field_es_data_table[LANGUAGE_NONE][0]['value']);
  $table = data_get_table($table_name);
  // SQL Table doesn't exist? Create it.
  if (!$table) {
    $title = !empty($source_file->field_es_data_table_title[LANGUAGE_NONE][0]['value']) ? $source_file->field_es_data_table_title[LANGUAGE_NONE][0]['value'] : $table_name;
    $table = _indicators_import_create_table($table_name, $title);
    if (!$table) {
      // @ignore security_3
      drupal_set_message(t('Could not create table @name. Please create it manually: !url',
        array(
          '@name' => $table_name,
          '!url' => filter_xss(l(t('admin/structure/data'), 'admin/structure/data')),
        )),
        'error'
      );
      return FALSE;
    }
  }

  // Empty table for the given source id before inserting data.
  $batch['operations'][] = array(
    '_indicators_import_data_proc_init', array(
      $table_name,
      $source_file->tid,
    ),
  );

  // Parse filter (for example "age=TOTAL").
  $filters = array();
  if (!empty($source_file->field_es_source_filters[LANGUAGE_NONE])) {
    foreach ($source_file->field_es_source_filters[LANGUAGE_NONE] as $filter_info) {
      // Allowed operators on filters.
      $operators = array('<=', '>=', '=', '<', '>');
      foreach ($operators as $operator) {
        // [0] = "age", [1] = "TOTAL"?
        $filter = explode($operator, $filter_info['value']);
        // Operator strictly matches?
        if (count($filter) == 2) {
          $filters[$filter[0]] = array('operator' => $operator, 'value' => $filter[1]);
          break;
        }
      }
    }
  }

  // Add mandatory vocabularies (they must all have the field "Code")
  $taxonomies = array(
    INDICATORS_VOC_FLAG => 'Eurostat Flags',
    INDICATORS_VOC_INDICATOR => 'Eurostat Indicators',
    INDICATORS_VOC_GEO => 'Eurostat Geo codes (NUTS)',
  );
  $fields = array();
  // Parse fields (for example "partner=taxonomy:es_geo:Geo codes")
  // and Add custom defined vocabularies.
  if (!empty($source_file->field_es_source_fields[LANGUAGE_NONE])) {
    foreach ($source_file->field_es_source_fields[LANGUAGE_NONE] as $field_info) {
      // "partner=taxonomy:es_geo:Geo codes:noindex"
      $field = $field_info['value'];
      // [0] = "partner", [1] = "taxonomy:es_geo:Geo codes:noindex"
      $field = explode('=', $field);
      $field_name = $field[0];
      $field_type = $field[1];
      $is_taxonomy = FALSE;
      $field_title = $field_name;
      $field_name = strtolower($field_name);
      $v = strpos($field_type, 'taxonomy:');
      if ($v !== FALSE) {
        // [0] = "es_geo", [1] = "Geo codes", [2] = "index".
        $t_info = explode(':', substr($field_type, $v + strlen('taxonomy:')));
        $t_info[0] = strtolower($t_info[0]);
        $field_title = isset($t_info[1]) ? $t_info[1] : $field_title;
        $field_type = 'taxonomy:' . $t_info[0];
        $is_taxonomy = TRUE;
        if (empty($source_file->field_es_source_format[LANGUAGE_NONE][0]['value'])
          || $source_file->field_es_source_format[LANGUAGE_NONE][0]['value'] == 'eurostat') {
          $taxonomies[$t_info[0]] = isset($t_info[1]) ? $t_info[1] : $t_info[0];
        }
      }
      else {
        // [0] = "varchar", [1] = "Time", [2] = "index".
        $t_info = explode(':', $field_type);
        $t_info[0] = strtolower($t_info[0]);
        $field_type = $t_info[0];
        $field_title = isset($t_info[1]) ? $t_info[1] : $field_title;
      }
      $create_index = isset($t_info[2]) ? ($t_info[2] == 'index' ? TRUE : FALSE) : FALSE;
      $fields[$field_name] = $field_type;
      // Field doesn't exist in Table? Add it.
      if (empty($table->table_schema['fields'][$field_name])) {
        $data_field_specs = data_get_field_definitions();
        // Taxonomy field?
        if ($is_taxonomy) {
          // To store tid.
          $field_type = 'unsigned int';
        }
        // Field is of a supported type, add it to the table.
        if (!empty($data_field_specs[$field_type])) {
          $tspec = $data_field_specs[$field_type];
          $tspec['description'] = $field_title;
          // Taxonomy field?
          if ($is_taxonomy) {
            $tspec['size'] = variable_get('indicators_table_int_size', 'normal');
          }
          $index_name = '';
          // If we have to create index, calculate its name.
          if ($create_index) {
            if ($is_taxonomy) {
              // Index group.
              $index_name = 'index_other';
            }
            else {
              $index_name = 'index_' . $field_name;
            }
          }
          // Add field to the table;
          // If taxonomy field, add index to 'index_other' group.
          _indicators_import_field_add($table, $field_name, $tspec, $index_name);
        }
        else {
          drupal_set_message(t('Unsupported source field type: %field', array('%field' => $field_info['value'])), 'error');
          return FALSE;
        }
      }
    }
  }

  // Source format (default 'eurostat') determines the function to call
  // to process a line from the source file.
  $source_format = isset($source_file->field_es_source_format[LANGUAGE_NONE][0]['value']) ? $source_file->field_es_source_format[LANGUAGE_NONE][0]['value'] : 'eurostat';

  // Statplanet file has lines with different meanings,
  // So they must be processed in on single batch.
  $batch_size = INDICATORS_IMPORT_DEFAULT_BATCH_SIZE;
  if ($source_format == 'statplanet') {
    $batch_size = PHP_INT_MAX;
  }

  module_load_include('inc', 'indicators_import', 'indicators_import.' . $source_format);
  $data_save_function = '_indicators_import_data_save_line_' . $source_format;
  if (!function_exists($data_save_function)) {
    drupal_set_message(t('Error importing %url : function %f does not exist.', array('%f' => $data_save_function, '%url' => $url)), 'error');
    return FALSE;
  }

  // CSV separators.
  $separators = array(
    'eurostat' => "\t",
    'statplanet' => ",",
  );

  // Put settings together:
  $settings = array(
    'source' => $source_file->tid,
    'fields' => $fields,
    'filters' => $filters,
    'taxonomies' => $taxonomies,
  );

  // Track execution time.
  $started = time();

  // Open remote file, unzip and store in temp.
  try {
    $temp_filename = '';
    $handle = indicators_fopen_gunzip($url, 'rb', $filename, $temp_filename);
  }
  catch (Exception $e) {
    drupal_set_message(t('Error opening %url : %message', array('%message' => $e->getMessage(), '%url' => $url)), 'error');
    return FALSE;
  }
  if (!$handle) {
    drupal_set_message(t('Error opening %url.', array('%url' => $url)), 'error');
    // Error message already displayed.
    return FALSE;
  }

  // Track execution time.
  $elapsed['unzip'] = time() - $started; $started = time();
  $row = 0; $start = 0;

  $batch_size = INDICATORS_IMPORT_DEFAULT_BATCH_SIZE;
  if ($source_format == 'statplanet') {
    $batch_size = PHP_INT_MAX;
  }

  try {
    while ((fgetcsv($handle, 4096, $separators[$source_format])) !== FALSE) {
      // After every X lines read (skip header).
      if (($row % $batch_size) == 1) {
        // Add a Batch callback to process the next X lines of the temp file.
        $batch['operations'][] = array(
          '_indicators_import_data_proc',
          array(
            $data_save_function,
            $table_name,
            $filename,
            $temp_filename,
            $settings,
            $row,
            $start,
            $source_format,
          ),
        );
      }
      $start = ftell($handle);
      ++$row;
    }
  }

  catch (Exception $e) {
    drupal_set_message(t('Error reading %url : %message', array('%message' => $e->getMessage(), '%url' => $url)), 'error');
    fclose($handle);
    return FALSE;
  }
  fclose($handle);

  /* Track execution time */
  $elapsed['parse'] = time() - $started; $started = time();

  // Init batch.
  batch_set($batch);

  /* Track execution time */
  $elapsed['batch_set'] = time() - $started; $started = time();
  watchdog(
    'indicators import',
    '%file copied and unzipped in %unzip seconds, parsed in %parse seconds, batch prepared in %batch_set seconds.' .
    ' Input file: %file, table: %table.',
    array(
      '%unzip' => $elapsed['unzip'],
      '%parse' => $elapsed['parse'],
      '%batch_set' => $elapsed['batch_set'],
      '%file' => $filename,
      '%table' => $table_name,
    )
  );
}


/**
 * Batch Callback: Initialize import processing.
 *
 *   Clear dataset from existing data etc.
 */
function _indicators_import_data_proc_init($table_name, $source_tid, &$context) {
  $table = data_get_table($table_name);
  if ($table) {
    $table->handler()->delete(array('source' => $source_tid));
  }
}

/**
 * Batch Callback: Import X lines of the input CSV file.
 */
function _indicators_import_data_proc($data_save_function, $table_name, $filename, $temp_filename, $settings, $l, $starting_ptr, $source_format, &$context) {
  // CSV separators.
  $separators = array(
    'eurostat' => "\t",
    'statplanet' => ",",
  );
  module_load_include('inc', 'indicators_import', 'indicators_import.' . $source_format);
  // Statplanet file has lines with different meanings,
  // So they must be processed in on single batch.
  $batch_size = INDICATORS_IMPORT_DEFAULT_BATCH_SIZE;
  if ($source_format == 'statplanet') {
    $batch_size = PHP_INT_MAX;
  }
  $lines = array();
  $records = 0;
  $skipped = 0;
  $row = 0;
  if (file_exists($filename)) {
    try {
      if (!isset($context['sandbox']['progress'])) {
        $context['sandbox']['progress'] = 0;
        $context['sandbox']['max'] = filesize($filename);
        // Load all needed taxonomies,
        // we only need correspondence between code and tid.
        foreach ($settings['taxonomies'] as $voc => $name) {
          $vocabularies[$voc] = _indicators_import_taxonomy_load($voc, $name);
        }
        $context['sandbox']['vocabularies'] = $vocabularies;
      }
      $settings['vocabularies'] = $context['sandbox']['vocabularies'];
      // Open the file again and go to the saved position.
      $handle = fopen($filename, 'r');
      if ($handle) {
        // Add header line.
        $line = fgetcsv($handle, 4096, $separators[$source_format]);
        if ($line !== FALSE) {
          // Add header.
          $lines[] = $line;
          ++$row;
        }
        // Go to the wanted position now.
        fseek($handle, $starting_ptr);
        // Gather up to X lines to process.
        while (($line = fgetcsv($handle, 4096, $separators[$source_format])) !== FALSE && $row <= $batch_size) {
          // Gather lines.
          $lines[] = $line;
          ++$row;
        }
        $starting_ptr = ftell($handle);
      }
    }
    catch (Exception $e) {
      drupal_set_message(t('Error reading %file : %message', array('%message' => $e->getMessage(), '%file' => $filename)), 'error');
      // There was an error, immediately complete the batch process.
      $context['finished'] = 1;
      return;
    }
  }
  else {
    drupal_set_message(t('Error reading %file : %message', array('%message' => t('File does not exist anymore.'), '%file' => $filename)), 'error');
    // There was an error, immediately complete the batch process.
    $context['finished'] = 1;
    return;
  }
  // Process gathered lines.
  if (count($lines)) {
    $starting_row = 0;
    // Call data save function for the given source format (Eurostat etc.)
    $table = data_get_table($table_name);
    if ($table) {
      $records += $data_save_function($table, $lines, $starting_row, $settings, $skipped);
    }
  }
  // Store some result for post-processing in the finished callback.
  if (empty($context['results'])) {
    $context['results'][] = array(
      $records,
      $skipped,
      $settings['source'],
      $filename,
      $temp_filename,
    );
  }
  else {
    $context['results'][] = array($records, $skipped);
  }
  // Update our progress information.
  $context['sandbox']['progress'] = $starting_ptr;
  $context['message'] = t('Now processing %x lines starting with line %line...', array('%x' => $batch_size, '%line' => $l));
}


/**
 * Batch Callback: Import finished.
 */
function _indicators_import_data_finished($success, $results, $operations) {
  if ($success) {
    $records = 0;
    $skipped = 0;
    // Sum up results.
    foreach ($results as $i => $result) {
      $records += $result[0];
      $skipped += $result[1];
      if ($i == 0) {
        $tid = $result[2];
        $filename = $result[3];
        $temp_filename = $result[4];
      }
    }
    if (!empty($tid)) {
      $source_file = taxonomy_term_load($tid);
      // Update Indicator's "updated" time.
      $updated = variable_get('indicators_import_updated', array());
      $updated[$tid] = time();
      variable_set('indicators_import_updated', $updated);
    }
    else {
      $source_file = new stdClass();
      $source_file->field_es_source_url[LANGUAGE_NONE][0]['value'] = 'unknown';
    }
    // Remove temp files.
    if (!empty($filename) && file_exists($filename)) {
      unlink($filename);
    }
    if (!empty($temp_filename) && file_exists($temp_filename)) {
      unlink($temp_filename);
    }
    // We just display the number of K lines we processed...
    $message = t('Processed @url. @records records inserted, @skipped records skipped.', array(
      '@url' => $source_file->field_es_source_url[LANGUAGE_NONE][0]['value'],
      '@records' => number_format($records),
      '@skipped' => number_format($skipped)
    ));
    drupal_set_message($message);
  }
  else {
    // An error occurred.
    // $operations contains the operations that remained unprocessed.
    $error_operation = reset($operations);
    drupal_set_message(t('An error occurred while processing @operation with arguments : @args',
      array(
        '@operation' => $error_operation[0],
        '@args' => print_r($error_operation[0], TRUE),
      )
    ));
  }
}


/**
 * HELPER function: Filter out the value.
 *
 * @param array $vars
 *   Query values, for example : array(0 => 'F', 1 => 'TOTAL', 2 => 'AT').
 * @param array $var_names
 *   Query variables, for example array('sex' => 0, 'age' => 1, 'geo' => 2).
 * @param array $filters
 *   Query filters, for example
 *   array('sex' => array('operator' => '=', 'value' => 'T')).
 *
 * @return bool
 *   TRUE = Skip, FALSE = Keep.
 */
function _indicators_import_filter_out(array &$vars, array &$var_names, array &$filters) {
  $skip = FALSE;
  foreach ($filters as $filter => $filter_info) {
    $value = $filter_info['value'];
    $operator = $filter_info['operator'];
    if ($operator == '=') {
      $operator = '==';
    }
    $comparison = "return '" . $vars[$var_names[$filter]] . "' $operator '" . $value . "';";
    if (!eval($comparison)) {
      $skip = TRUE;
      break;
    }
  }
  return $skip;
}

/**
 * Get correspondence between codes and their respective tid.
 *
 * @param string $voc_machine_name
 *   Vocabulary info.
 * @param string $voc_name
 *   Vocabulary info.
 *
 * @return array
 *   Array (code => tid).
 */
function _indicators_import_taxonomy_load($voc_machine_name, $voc_name) {
  $voc = taxonomy_vocabulary_machine_name_load($voc_machine_name);
  // Vocabulary doesn't exist? create it.
  if (!$voc) {
    // Create a term in Dictionaries vocabulary.
    $source_file = _indicators_create_dic_term($voc_machine_name, $voc_name);
    // Create corresponding vocabulary.
    $voc = _indicators_import_create_voc($voc_machine_name, $voc_name);
    // Import dictionary.
    module_load_include('inc', 'indicators_import', 'indicators_import.dic.admin');
    $res = _indicators_import_dic_import_save($source_file);
    $records = is_array($res) ? $res[1] : 0;
    drupal_set_message(t('Vocabulary %name imported %number records.', array('%name' => $voc_name, '%number' => $records)));
  }
  $voc_tree = taxonomy_get_tree($voc->vid, 0, NULL, TRUE);
  $taxonomy = array();
  foreach ($voc_tree as $term) {
    if (!empty($term->field_es_code[LANGUAGE_NONE][0]['value'])) {
      $taxonomy[$term->field_es_code[LANGUAGE_NONE][0]['value']] = $term->tid;
    }
  }
  return $taxonomy;
}

/**
 * Validate a dataset source item.
 *
 * To be validated:
 *   - dataset SQL table must exist
 *   - remote files must exist
 *   - vocabularies must exist
 *
 * @param array $datasets
 *   List of SQL tables.
 * @param object $indicator
 *   Term.
 *
 * @return string
 *   Error message or '' if ok.
 *�*/
function _indicators_import_validate(array &$datasets, $indicator) {
  $messages = array();
  $warnings = array();
  $fields = array(
    'field_es_code' => t('code'),
    'field_es_data_table' => t('data table'),
    'field_es_source_url' => t('source url'),
    'field_es_source_filename' => t('source file name'),
  );
  // All fields should be set?
  foreach ($fields as $field => $name) {
    if (isset($indicator->{$field})) {
      if (empty($indicator->{$field}[LANGUAGE_NONE][0]['value'])) {
        $messages[] = t('Dataset !field is not set.', array('!field' => $name));
      }
      // Check existence of remote file.
      else {
        $url = $indicator->{$field}[LANGUAGE_NONE][0]['value'];
        if (substr($url, 0, 7) == 'http://' || substr($url, 0, 8) == 'https://') {
          if (!_indicators_import_fileexists($url)) {
            $messages[] = t('Url !url in %field is not accessible.', array('!url' => $url, '%field' => $name));
          }
        }
      }
    }
  }

  // If all fields are set, check fields' content.
  if (empty($messages)) {
    // Does SQL table exist?
    if (isset($indicator->field_es_data_table) && empty($datasets[$indicator->field_es_data_table[LANGUAGE_NONE][0]['value']])) {
      $warnings[] = t('Dataset %name does not exist.', array('%name' => $indicator->field_es_data_table[LANGUAGE_NONE][0]['value']));
    }
  }

  // Compile final message if any errors.
  if (!empty($messages)) {
    return ' <span class="error"><em>' . t('Error') . '</em> : ' . implode('. ', $messages) . '</span>';
  }
  elseif (!empty($warnings)) {
    return ' <span class="warning"><em>' . t('Warning') . '</em> : ' . implode('. ', $warnings) . '</span>';
  }

  return '';
}

/**
 * Create a Data table.
 *
 * Mandatory fields:
 *   - eid
 *   - geo
 *   - value
 *   - flag
 *   - time
 *   - source
 *
 * @param string $name
 *   SQL Data table name.
 * @param string $title
 *   SQL Data table title.
 *
 * @return object
 *   Created Data table object or FALSE
 *
 * @see https://drupal.org/node/159605
 * @see http://dev.mysql.com/doc/refman/5.0/en/integer-types.html
 */
function _indicators_import_create_table($name, $title) {
  if (!$title) {
    $title = $name;
  }
  $data_table = new stdClass();
  $data_table->disabled = FALSE; /* Edit this to true to make a default data_table disabled initially */
  $data_table->api_version = 1;
  $data_table->title = $title;
  $data_table->name = $name;
  $data_table->table_schema = array(
    'description' => '',
    'fields' => array(
      'eid' => array(
        'type' => 'serial',
        'size' => 'normal',
        'not null' => TRUE,
        'description' => 'The primary identifier for a record item.',
      ),
      'source' => array(
        'type' => 'int',
        'size' => variable_get('indicators_table_int_size', 'normal'),
        'unsigned' => TRUE,
        'description' => 'The tid of the Indicator taxonomy (es_indicator).',
      ),
      'geo' => array(
        'type' => 'int',
        'size' => variable_get('indicators_table_int_size', 'normal'),
        'unsigned' => TRUE,
        'not null' => FALSE,
        'description' => 'The tid of the Geographical region taxonomy (es_geo) for the record.',
      ),
      'value' => array(
        'type' => 'float',
        'size' => 'big',
        'not null' => FALSE,
        'description' => 'Value of a record item.',
      ),
      'flag' => array(
        'type' => 'int',
        'size' => variable_get('indicators_table_int_size', 'normal'),
        'unsigned' => TRUE,
        'not null' => FALSE,
        'description' => 'The tid of the Flag taxonomy (es_flag) for the record.',
      ),
      'time' => array(
        'type' => 'varchar',
        'size' => 'normal',
        'length' => '255',
        'not null' => FALSE,
        'description' => 'Time of the record item (usually Year).',
      ),
    ),
    'name' => $name,

    // Indexes.
    'indexes' => array(
      'index_main' => array('source', 'geo', 'time'),
    ),
    // For Drupal info, they won't be created in the DB.
    'foreign keys' => array(
      'source_tid' => array(
        'table' => 'taxonomy_term_data',
        'columns' => array('source' => 'tid'),
      ),
      'geo_tid' => array(
        'table' => 'taxonomy_term_data',
        'columns' => array('geo' => 'tid'),
      ),
      'flag_tid' => array(
        'table' => 'taxonomy_term_data',
        'columns' => array('flag' => 'tid'),
      ),
    ),
    // Primary Key.
    'primary key' => array('eid'),
    // Optimal DB engine.
    'mysql_engine' => variable_get('indicators_mysql_engine', 'MyISAM'),
  );
  $data_table->meta = array(
    'fields' => array(
      'eid' => array(
        'label' => 'Entry ID',
      ),
      'geo' => array(
        'label' => 'Geo',
      ),
      'value' => array(
        'label' => 'Value',
      ),
      'flag' => array(
        'label' => 'Flag',
      ),
      'time' => array(
        'label' => 'Time',
      ),
      'source' => array(
        'label' => 'Source',
      ),
    ),
  );
  $table = data_create_table($name, $data_table->table_schema, $title);
  if ($table) {
    // Add meta data.
    $table->update(array('meta' => $data_table->meta));
    // Clear cache.
    $table->clearCaches();
  }
  return $table;
}

/**
 * HELPER FUNCTION: Add a new field to a Data table.
 *
 * @param object $table
 *   Data table.
 * @param string $field_name
 *   Field info.
 * @param array $field_info
 *   Field info.
 * @param string $index
 *   Group name for the index to create on the field or '' for none.
 */
function _indicators_import_field_add($table, $field_name, array $field_info, $index = '') {
  $table->addField($field_name, $field_info);
  // Create index on the field?
  if ($index) {
    // If the index doesn't exist already in the group or group doesn't exist.
    if (!isset($table->table_schema['indexes'][$index]) ||
        (isset($table->table_schema['indexes'][$index]) && !in_array($field_name, $table->table_schema['indexes'][$index]))) {
      // Drop group of indexes.
      db_drop_index($table->name, $index);
      // Add index to the group.
      $table->table_schema['indexes'][$index][] = $field_name;
      // Add index to the DB.
      db_add_index($table->name, $index, $table->table_schema['indexes'][$index]);
      // Rebuild cache.
      drupal_get_schema($table->name, TRUE);
    }
  }
  $table->table_schema['fields'][$field_name] = $field_info;
  $table->meta['fields'][$field_name] = array(
    'label' => $field_info['description'],
  );
  $table->update(array('table_schema' => $table->table_schema, 'meta' => $table->meta));
  $table->clearCaches();
  drupal_set_message(t('Added field %field to Data table %name', array('%field' => $field_name, '%name' => $table->title)));
}
