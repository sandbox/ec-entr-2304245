<?php

/**
 * @file
 * Default theme implementation to display a statistical data block.
 *
 * Available variables:
 * - $help: Additional text to display.
 * - $content: Statistical data (themed html string)
 * - $info: Info about the executed Query (array):
 *     - filters (array)
 *     - pager (array)
 *     - dimensions (int)
 *     - axis (array)
 * - $conf: Panel configuration vars
 *
 * @ingroup themeable
 */
 if ($content && substr(trim($content), 0, 3) != '<td'):
   $content = '<td>' . $content . '</td>';
 endif;
 $css_id = (!empty($conf['css_id'])) ? ' id="' . $conf['css_id'] . '"' : '';
?>
<?php if ($content): ?>
  <?php if ($help): ?>
    <tr class="help"><td><?php print $help; ?></td></tr>
  <?php endif;?>
  <tr class="es-qd-data <?php print $conf['display']; ?>"<?php print $css_id; ?>>
    <?php print $content; ?>
  </tr>
<?php endif; ?>
